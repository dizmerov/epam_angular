## About application running:
1.You must add ojdbc.jar to server lib folder. Dependency ojdbc with scope provided is set in pom.xml because of problem with unregister jdbc driver by tomcat server:
```
#!xml

   <dependency>
       <groupId>com.oracle</groupId>
       <artifactId>ojdbc6</artifactId>
       <version>${oracle.version}</version>
       <scope>provided</scope>
   </dependency>
```
2.If you use oracle db, then you must set -Duser.country|region = en|your_value, -Duser.language = en|your_value in VM arguments for tomcat server running in order to 
	there are no errors when connection will be created by application.

3.You choose db type by setting predefined value in parameter spring.profiles.default in web.xml.