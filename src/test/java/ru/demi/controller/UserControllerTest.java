package ru.demi.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.demi.UserTestData.ADMIN;
import static ru.demi.UserTestData.CONVERTER;
import static ru.demi.UserTestData.CONVERTER_TO;
import static ru.demi.UserTestData.MATCHER;
import static ru.demi.UserTestData.MATCHER_TO;
import static ru.demi.UserTestData.NEW_USER;
import static ru.demi.UserTestData.NEW_USER_AFTER_SAVE;
import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.USER2;
import static ru.demi.UserTestData.USER_FOR_SAVE;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;

import ru.demi.Matcher;
import ru.demi.exception.UserIsNotDeletedException;
import ru.demi.exception.UserNotFoundException;
import ru.demi.service.UserService;
import ru.demi.to.LoggedUser;
import ru.demi.util.JSONResponse;

public class UserControllerTest extends AbstractControllerTest {

    public static final String REST_URL = UserController.REST_URL + '/';

    public static final Matcher<JSONResponse> RESPONSE_MATCHER = new Matcher<JSONResponse>(JSONResponse.class);

    @Autowired
    private UserService service;

    @Before
    public void setUp() {
        reset(service);
    }

    @Test
    public void whenGetById() throws Exception {
        when(service.findById(USER1.getId())).thenReturn(USER1);

        mockMvc.perform(get(REST_URL + USER1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(ADMIN))))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentMatcher(CONVERTER.apply(USER1), objectMapper));

        verify(service).findById(USER1.getId());
    }

    @Test
    public void whenGetByIdIsNotFound() throws Exception {
        int userId = 100;
        doThrow(new UserNotFoundException()).when(service).findById(userId);

        mockMvc.perform(get(REST_URL + userId)
            .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(ADMIN))))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).findById(userId);
    }

    @Test
    public void whenGetByIdAsUsualUser() throws Exception {
        mockMvc.perform(get(REST_URL + USER1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(USER2))))
            .andDo(print())
            .andExpect(status().isForbidden());

        verify(service, never()).findById(USER1.getId());
    }

    @Test
    public void whenGetByIdWithoutAuth() throws Exception {
        mockMvc.perform(get(REST_URL + 1))
            .andDo(print())
            .andExpect(status().isUnauthorized());

        verify(service, never()).findById(USER1.getId());
    }

    @Test
    public void whenUpdateUser() throws Exception {
        when(service.save(USER_FOR_SAVE)).thenReturn(USER_FOR_SAVE);

        mockMvc.perform(
                post(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(ADMIN)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(USER_FOR_SAVE))
            )
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).save(USER_FOR_SAVE);
    }

    @Test
    public void whenUpdateAsUsualUser() throws Exception {

        mockMvc.perform(
                post(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(USER2)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(USER_FOR_SAVE))
            )
            .andDo(print())
            .andExpect(status().isForbidden());

        verify(service, never()).save(USER_FOR_SAVE);
    }

    @Test
    public void whenUpdateWithoutAuth() throws Exception {

        mockMvc.perform(
                post(REST_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(USER_FOR_SAVE))
            )
            .andDo(print())
            .andExpect(status().isUnauthorized());

        verify(service, never()).save(USER_FOR_SAVE);
    }

    @Test
    public void whenAddUser() throws Exception {
        when(service.save(NEW_USER)).thenReturn(NEW_USER_AFTER_SAVE);

        mockMvc.perform(
                put(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(ADMIN)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(NEW_USER))
            )
            .andDo(print())
            .andExpect(status().isCreated());

        verify(service).save(NEW_USER);
    }

    @Test
    public void whenAddAsUsualUser() throws Exception {

        mockMvc.perform(
                put(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(USER2)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(NEW_USER))
            )
            .andDo(print())
            .andExpect(status().isForbidden());

        verify(service, never()).save(NEW_USER);
    }

    @Test
    public void whenAddWithoutAuth() throws Exception {

        mockMvc.perform(
                put(REST_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(NEW_USER))
            )
            .andDo(print())
            .andExpect(status().isUnauthorized());

        verify(service, never()).save(NEW_USER);
    }

    @Test
    public void whenDeleteExistentUser() throws Exception {
        doNothing().when(service).delete(USER1.getId());

        mockMvc.perform(delete(REST_URL + USER1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(ADMIN))))
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).delete(USER1.getId());
    }

    @Test
    public void whenDeleteNonExistentUser() throws Exception {

        int userId = 100;
        doThrow(new UserIsNotDeletedException()).when(service).delete(userId);

        mockMvc.perform(delete(REST_URL + userId)
            .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(ADMIN))))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).delete(userId);
    }

    @Test
    public void whenDeleteAsUsualUser() throws Exception {
        mockMvc.perform(delete(REST_URL + USER1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(new LoggedUser(USER2))))
            .andDo(print())
            .andExpect(status().isForbidden());

        verify(service, never()).delete(USER1.getId());
    }

    @Test
    public void whenDeleteUserWithoutAuth() throws Exception {
        mockMvc.perform(delete(REST_URL + 1))
            .andDo(print())
            .andExpect(status().isUnauthorized());

        verify(service, never()).delete(USER1.getId());
    }

    @Test
    public void whenExistsLogin() throws Exception {
        String login = "login1";
        when(service.exists(login)).thenReturn(true);
        mockMvc.perform(
                get(REST_URL + "exists")
                .param("login", login)
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(RESPONSE_MATCHER.contentMatcher(new JSONResponse(Boolean.TRUE), objectMapper));

        verify(service).exists(login);
    }

    @Test
    public void whenNotExistsLogin() throws Exception {
        String login = "invalid login";
        when(service.exists(login)).thenReturn(false);
        mockMvc.perform(
                get(REST_URL + "exists")
                .param("login", login)
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(RESPONSE_MATCHER.contentMatcher(new JSONResponse(Boolean.FALSE), objectMapper));

        verify(service).exists(login);
    }

    @Test
    public void whenFoundByLogin() throws Exception {
        when(service.findByLogin(USER1.getLogin())).thenReturn(USER1);
        mockMvc.perform(
                get(REST_URL + "by")
                .param("login", USER1.getLogin())
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER_TO.contentMatcher(CONVERTER_TO.apply(USER1), objectMapper));

        verify(service).findByLogin(USER1.getLogin());
    }

    @Test
    public void whenNotFoundByLogin() throws Exception {
        String login = "invalid login";
        doThrow(new UserNotFoundException()).when(service).findByLogin(login);
        mockMvc.perform(
                get(REST_URL + "by")
                .param("login", login)
            )
            .andDo(print())
            .andExpect(status().isNotFound());
    }
}
