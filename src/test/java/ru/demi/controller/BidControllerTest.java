package ru.demi.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.demi.BidTestData.BID1;
import static ru.demi.BidTestData.BID3;
import static ru.demi.BidTestData.CONVERTER;
import static ru.demi.BidTestData.MATCHER;
import static ru.demi.BidTestData.NEW_BID_AFTER_SAVE;
import static ru.demi.ProductTestData.PRODUCT1;
import static ru.demi.UserTestData.GUEST;
import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.USER2;
import static ru.demi.UserTestData.toLoggedUser;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;

import ru.demi.exception.BidsAreNotDeletedException;
import ru.demi.exception.BidNotFoundException;
import ru.demi.exception.ProductIsNotDeletedException;
import ru.demi.service.BidService;

public class BidControllerTest extends AbstractControllerTest {

    public static final String REST_URL = BidController.REST_URL + '/';

    @Autowired
    private BidService service;

    @Before
    public void setUp() {
        reset(service);
    }

    @Test
    public void whenGetById() throws Exception {

        when(service.findById(BID1.getId(), USER1.getId())).thenReturn(BID1);

        mockMvc.perform(get(REST_URL + BID1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentMatcher(CONVERTER.apply(BID1), objectMapper));

        verify(service).findById(BID1.getId(), BID1.getBidder().getId());
    }

    @Test
    public void whenGetByIdIsNotFound() throws Exception {
        int bidId = 100;
        doThrow(new BidNotFoundException()).when(service).findById(bidId, USER1.getId());

        mockMvc.perform(get(REST_URL + bidId)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).findById(bidId, USER1.getId());
    }

    @Test
    public void whenGetByIdAsGuest() throws Exception {
        mockMvc.perform(get(REST_URL + BID1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(GUEST))))
            .andDo(print())
            .andExpect(status().isForbidden());
    }

    @Test
    public void whenGetByIdWithoutAuth() throws Exception {
        mockMvc.perform(get(REST_URL + BID1.getId()))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenGetAll() throws Exception {
        when(service.findAllByBidderId(USER1.getId())).thenReturn(Arrays.asList(BID1, BID3));

        mockMvc.perform(get(REST_URL)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentSetMatcher(objectMapper, CONVERTER.apply(BID1), CONVERTER.apply(BID3)));

        verify(service).findAllByBidderId(USER1.getId());
    }

    @Test
    public void whenGetAllAsGuest() throws Exception {
        mockMvc.perform(get(REST_URL)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(GUEST))))
            .andDo(print())
            .andExpect(status().isForbidden());
    }

    @Test
    public void whenGetAllWithoutAuth() throws Exception {
        mockMvc.perform(get(REST_URL))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenDeleteExistentBid() throws Exception {
        doNothing().when(service).delete(BID1.getId(), USER1.getId());

        mockMvc.perform(delete(REST_URL + BID1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).delete(BID1.getId(), USER1.getId());
    }

    @Test
    public void whenDeleteNonExistentBid() throws Exception {
        int bidId = 100;
        doThrow(new BidsAreNotDeletedException()).when(service).delete(bidId, USER1.getId());

        mockMvc.perform(delete(REST_URL + bidId)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).delete(bidId, USER1.getId());
    }

    @Test
    public void whenDeleteBidAsGuest() throws Exception {
        mockMvc.perform(delete(REST_URL + BID1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(GUEST))))
            .andDo(print())
            .andExpect(status().isForbidden());
    }

    @Test
    public void whenDeleteBidWithoutAuth() throws Exception {
        mockMvc.perform(delete(REST_URL + BID1.getId()))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenBidForExistentProduct() throws Exception {
        long offer = NEW_BID_AFTER_SAVE.getOffer();
        when(service.bidForProduct(USER2.getId(), PRODUCT1.getId(), offer)).thenReturn(NEW_BID_AFTER_SAVE);

        mockMvc.perform(
                post(REST_URL + "/products/" + PRODUCT1.getId())
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER2)))
                .param("offer", Long.toString(offer))
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(MATCHER.contentMatcher(CONVERTER.apply(NEW_BID_AFTER_SAVE), objectMapper));

        verify(service).bidForProduct(USER2.getId(), PRODUCT1.getId(), offer);
    }

    @Test
    public void whenBidForNonExistentProduct() throws Exception {
        int productId = 100;
        long offer = 1000;
        doThrow(new ProductIsNotDeletedException()).when(service).bidForProduct(USER1.getId(), productId, offer);

        mockMvc.perform(
                post(REST_URL + "/products/" + productId)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
                .param("offer", Long.toString(offer))
            )
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).bidForProduct(USER1.getId(), productId, offer);
    }

    @Test
    public void whenBidForProductWithoutAuth() throws Exception {
        mockMvc.perform(post(REST_URL + "/products/" + 1))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }
}
