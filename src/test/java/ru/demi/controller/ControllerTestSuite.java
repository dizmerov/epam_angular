package ru.demi.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    BidControllerTest.class,
    ProductControllerTest.class,
    UserControllerTest.class
})
public class ControllerTestSuite {}