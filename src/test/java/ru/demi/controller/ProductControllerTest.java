package ru.demi.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.demi.ProductTestData.CONVERTER;
import static ru.demi.ProductTestData.MATCHER;
import static ru.demi.ProductTestData.NEW_PRODUCT;
import static ru.demi.ProductTestData.NEW_PRODUCT_AFTER_SAVE;
import static ru.demi.ProductTestData.PRODUCT1;
import static ru.demi.ProductTestData.PRODUCT3;
import static ru.demi.ProductTestData.PRODUCTS;
import static ru.demi.ProductTestData.PRODUCT_FOR_SAVE;
import static ru.demi.UserTestData.GUEST;
import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.toLoggedUser;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;

import ru.demi.Matcher;
import ru.demi.exception.ProductIsNotDeletedException;
import ru.demi.exception.ProductNotFoundException;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.service.ProductService;

public class ProductControllerTest extends AbstractControllerTest {

    public static final String REST_URL = ProductController.REST_URL + '/';

    @Autowired
    private ProductService service;

    @Before
    public void setUp() {
        reset(service);
    }

    @Test
    public void whenGetById() throws Exception {
        when(service.findById(PRODUCT1.getId(), USER1.getId())).thenReturn(PRODUCT1);

        mockMvc.perform(get(REST_URL + 1)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentMatcher(CONVERTER.apply(PRODUCT1), objectMapper));

        verify(service).findById(PRODUCT1.getId(), USER1.getId());
    }

    @Test
    public void whenGetByIdNonExistentProduct() throws Exception {
        int productId = 100;
        doThrow(new ProductNotFoundException()).when(service).findById(productId, USER1.getId());

        mockMvc.perform(get(REST_URL + productId)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).findById(productId, USER1.getId());
    }

    @Test
    public void whenGetByIdWithoutAuth() throws Exception {
        mockMvc.perform(get(REST_URL + 1))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenGetAll() throws Exception {
        boolean wOthers = true;
        when(service.findAllByParams(null, null, USER1.getId(), wOthers)).thenReturn(new ArrayList<>(PRODUCTS));

        mockMvc.perform(
                get(REST_URL)
                .param("withOthers", "on")
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentSetMatcher(Matcher.map(PRODUCTS, CONVERTER), objectMapper));

        verify(service).findAllByParams(null, null, USER1.getId(), wOthers);
    }

    @Test
    public void whenGetAllWithId() throws Exception {
        boolean wOthers = false;
        when(service.findAllByParams(ProductService.ID_PARAM, Integer.toString(PRODUCT1.getId()), USER1.getId(), wOthers)).thenReturn(Arrays.asList(PRODUCT1));

        mockMvc.perform(
                get(REST_URL)
                .param("id", Integer.toString(PRODUCT1.getId()))
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentSetMatcher(objectMapper, CONVERTER.apply(PRODUCT1)));

        verify(service).findAllByParams(ProductService.ID_PARAM, Integer.toString(PRODUCT1.getId()), USER1.getId(), wOthers);
    }

    @Test
    public void whenGetAllWithTitle() throws Exception {
        String title = "product";
        boolean wOthers = false;
        when(service.findAllByParams(ProductService.TITLE_PARAM, title, USER1.getId(), wOthers)).thenReturn(Arrays.asList(PRODUCT1));

        mockMvc.perform(
                get(REST_URL)
                .param("title", title)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentSetMatcher(objectMapper, CONVERTER.apply(PRODUCT1)));

        verify(service).findAllByParams(ProductService.TITLE_PARAM, title, USER1.getId(), wOthers);
    }

    @Test
    public void whenGetAllWithDesc() throws Exception {
        String desc = "product";
        boolean wOthers = false;
        when(service.findAllByParams(ProductService.DESC_PARAM, desc, USER1.getId(), wOthers)).thenReturn(Arrays.asList(PRODUCT1));

        mockMvc.perform(
                get(REST_URL)
                .param("desc", desc)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MATCHER.contentSetMatcher(objectMapper, CONVERTER.apply(PRODUCT1)));

        verify(service).findAllByParams(ProductService.DESC_PARAM, desc, USER1.getId(), wOthers);
    }

    @Test
    public void whenUpdateProduct() throws Exception {
        when(service.save(PRODUCT_FOR_SAVE, PRODUCT_FOR_SAVE.getOwner().getId())).thenReturn(PRODUCT_FOR_SAVE);

        mockMvc.perform(
                post(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(PRODUCT_FOR_SAVE))
            )
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).save(PRODUCT_FOR_SAVE, PRODUCT_FOR_SAVE.getOwner().getId());
    }

    @Test
    public void whenUpdateProductAsGuest() throws Exception {
        mockMvc.perform(
                post(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(GUEST)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(PRODUCT_FOR_SAVE))
            )
            .andDo(print())
            .andExpect(status().isForbidden());
    }

    @Test
    public void whenUpdateProductWithoutAuth() throws Exception {

        mockMvc.perform(
                post(REST_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(PRODUCT_FOR_SAVE))
            )
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenAddProduct() throws Exception {
        when(service.save(NEW_PRODUCT, USER1.getId())).thenReturn(NEW_PRODUCT_AFTER_SAVE);

        mockMvc.perform(
                put(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(NEW_PRODUCT))
            )
            .andDo(print())
            .andExpect(status().isCreated());

        verify(service).save(NEW_PRODUCT, USER1.getId());
    }

    @Test
    public void whenAddProductAsGuest() throws Exception {
        mockMvc.perform(
                put(REST_URL)
                .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(GUEST)))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(NEW_PRODUCT))
            )
            .andDo(print())
            .andExpect(status().isForbidden());
    }

    @Test
    public void whenAddProductWithoutAuth() throws Exception {
        mockMvc.perform(
                put(REST_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(NEW_PRODUCT))
            )
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenDeleteExistentProduct() throws Exception {
        doNothing().when(service).delete(PRODUCT1.getId(), USER1.getId());

        mockMvc.perform(delete(REST_URL + PRODUCT1.getId())
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).delete(PRODUCT1.getId(), USER1.getId());
    }

    @Test
    public void whenDeleteNonExistentProduct() throws Exception {
        int productId = 100;
        doThrow(new ProductIsNotDeletedException()).when(service).delete(productId, USER1.getId());

        mockMvc.perform(delete(REST_URL + productId)
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        verify(service).delete(productId, USER1.getId());
    }

    @Test
    public void whenDeleteProductWithoutAuth() throws Exception {
        mockMvc.perform(delete(REST_URL + PRODUCT1.getId()))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenSellProduct() throws Exception {
        Product productForSale = new ProductItem(PRODUCT1, ProductStatus.FOR_SALE);
        when(service.sellProduct(PRODUCT1.getId(), USER1.getId())).thenReturn(productForSale);

        mockMvc.perform(post(REST_URL + PRODUCT1.getId() + "/sell")
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).sellProduct(PRODUCT1.getId(), USER1.getId());
    }

    @Test
    public void whenSellProductWithoutAuth() throws Exception {
        mockMvc.perform(post(REST_URL + PRODUCT1.getId() + "/sell"))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void whenBuyProductNow() throws Exception {
        Product boughtProduct = new ProductItem(PRODUCT3, ProductStatus.NOT_FOR_SALE, USER1);
        when(service.buyProduct(USER1.getId(), PRODUCT3.getId(), false)).thenReturn(boughtProduct);

        mockMvc.perform(post(REST_URL + PRODUCT3.getId() + "/buy")
            .with(SecurityMockMvcRequestPostProcessors.user(toLoggedUser(USER1))))
            .andDo(print())
            .andExpect(status().isOk());

        verify(service).buyProduct(USER1.getId(), PRODUCT3.getId(), false);
    }

    @Test
    public void whenBuyProductNowWithoutAuth() throws Exception {
        mockMvc.perform(post(REST_URL + PRODUCT1.getId() + "/buy"))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }
}
