package ru.demi.repository;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ru.demi.Profiles;

@ContextConfiguration({
    "classpath:spring/spring-db.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles({Profiles.HSQLDB, Profiles.JPA})
@Sql(scripts = "classpath:db/populateDB_hsqldb.sql", config = @SqlConfig(encoding = "UTF-8")) // ${database.populateLocation}
abstract public class AbstractRepositoryTest {}
