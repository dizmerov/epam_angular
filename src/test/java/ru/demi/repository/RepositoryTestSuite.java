package ru.demi.repository;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    BidRepositoryTest.class,
    ProductRepositoryTest.class,
    UserRepositoryTest.class
})
public class RepositoryTestSuite {}