package ru.demi.repository;

import static org.junit.Assert.assertEquals;
import static ru.demi.BidTestData.BID1;
import static ru.demi.BidTestData.BID2;
import static ru.demi.BidTestData.BID3;
import static ru.demi.BidTestData.NEW_BID;
import static ru.demi.BidTestData.NEW_BID_AFTER_SAVE;
import static ru.demi.ProductTestData.PRODUCT3;
import static ru.demi.UserTestData.USER1;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import ru.demi.exception.BidNotFoundException;
import ru.demi.model.Bid;
import ru.demi.model.BidItem;

@Transactional
@Rollback(false)
public class BidRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private BidRepository bidRepository;

    @BeforeClass
    public static void beforeClass() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void whenFoundBidById() {
        Bid foundBid = bidRepository.findById(BID1.getId(), USER1.getId());
        assertEquals(BID1, foundBid);
    }

    @Test(expected = BidNotFoundException.class)
    public void whenNotFoundBidById() {
        bidRepository.findById(110, 1);
    }

    @Test
    public void whenFoundBestBidForProduct() {
        Bid foundBid = bidRepository.findBestBid(PRODUCT3.getId(), PRODUCT3.getFinishDate());
        assertEquals(BID2, foundBid);
    }

    @Test(expected = BidNotFoundException.class)
    public void whenNotFoundBestBid() {
        bidRepository.findBestBid(110, LocalDateTime.now());
    }

    @Test
    public void whenFoundAllBidsForProduct() {
        Set<Bid> bids = new HashSet<>(Arrays.asList(BID1, BID2));
        assertEquals(bids, new HashSet<>(bidRepository.findAllByProductId(PRODUCT3.getId())));
    }

    @Test
    public void whenFoundAllByBidderId() {
        Set<Bid> bids = new HashSet<>(Arrays.asList(BID1, BID3));
        assertEquals(bids, new HashSet<>(bidRepository.findAllByBidderId(USER1.getId())));
    }

    @Rollback(true)
    @Test
    public void whenCreatedNewBid() throws SQLException {
        Set<Bid> bids = new HashSet<>(Arrays.asList(BID1, BID2, NEW_BID_AFTER_SAVE));
        bidRepository.save(NEW_BID);
        assertEquals(bids, new HashSet<>(bidRepository.findAllByProductId(PRODUCT3.getId())));
    }


    @Rollback(true)
    @Test(expected = BidNotFoundException.class)
    public void whenBidIsDeleted() {
        bidRepository.delete(BID1.getId(), BID1.getBidder().getId());
        bidRepository.findById(BID1.getId(), BID1.getBidder().getId());
    }

    @Rollback(true)
    @Test
    public void whenBidsIsDeactivated() {
        Set<Bid> bids = new HashSet<>(Arrays.asList(new BidItem(BID1, false), new BidItem(BID2, false)));
        bidRepository.deactivateBids(Arrays.asList(PRODUCT3.getId()));
        assertEquals(bids, new HashSet<>(bidRepository.findAllByProductId(PRODUCT3.getId())));
    }
}
