package ru.demi.repository;

import static org.junit.Assert.assertEquals;
import static ru.demi.UserTestData.NEW_USER;
import static ru.demi.UserTestData.NEW_USER_AFTER_SAVE;
import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.USERS;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import ru.demi.exception.UserNotFoundException;
import ru.demi.model.User;
import ru.demi.model.UserItem;

@Transactional
@Rollback(false)
public class UserRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @BeforeClass
    public static void beforeClass() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void whenFoundUserById() {
        assertEquals(USER1, userRepository.findById(1));
    }

    @Test(expected = UserNotFoundException.class)
    public void whenNotFoundUserById() {
        userRepository.findById(10);
    }

    @Test
    public void whenFoundUserByLogin() {
        assertEquals(USER1, userRepository.findByLogin(USER1.getLogin()));
    }

    @Test(expected = UserNotFoundException.class)
    public void whenNotFoundUserByLogin() {
        userRepository.findByLogin("invalid login");
    }

    @Test
    public void whenFoundUserByLoginAndPassword() {
        assertEquals(USER1, userRepository.findByLoginAndPassword(USER1.getLogin(), USER1.getPassword()));
    }

    @Test(expected = UserNotFoundException.class)
    public void whenNotFoundUserByLoginAndPassword() {
        userRepository.findByLoginAndPassword("some login", "password");
    }

    @Test
    public void whenFoundAllUser() {
        assertEquals(USERS, new HashSet<>(userRepository.findAll()));
    }

    @Rollback(true)
    @Test
    public void whenCreatedNewUser() {
        userRepository.save(NEW_USER);
        assertEquals(NEW_USER_AFTER_SAVE, userRepository.findById(NEW_USER_AFTER_SAVE.getId()));
    }

    @Rollback(true)
    @Test
    public void whenChangedUserData() throws SQLException {
        int id = 1;
        User userForSaving = userRepository.findById(id);
        userForSaving = new UserItem(userForSaving, "newPassword");
        userRepository.save(userForSaving);
        User savedUser = userRepository.findById(id);
        assertEquals(userForSaving.getPassword(), savedUser.getPassword());
    }

    @Rollback(true)
    @Test(expected = UserNotFoundException.class)
    public void whenUserIsDeleted() {
        userRepository.delete(USER1.getId());
        userRepository.findById(USER1.getId());
    }
}
