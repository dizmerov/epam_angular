package ru.demi.repository;

import static org.junit.Assert.assertEquals;
import static ru.demi.ProductTestData.NEW_PRODUCT;
import static ru.demi.ProductTestData.NEW_PRODUCT_AFTER_SAVE;
import static ru.demi.ProductTestData.PRODUCT1;
import static ru.demi.ProductTestData.PRODUCT2;
import static ru.demi.ProductTestData.PRODUCT3;
import static ru.demi.ProductTestData.PRODUCT4;
import static ru.demi.ProductTestData.PRODUCTS;
import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.USER3;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import ru.demi.exception.ProductNotFoundException;
import ru.demi.model.ProductStatus;
import ru.demi.service.ProductService;

@Transactional
@Rollback(false)
public class ProductRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @BeforeClass
    public static void beforeClass() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void whenProductIsFoundById() {
        assertEquals(PRODUCT1, productRepository.findById(PRODUCT1.getId()));
    }

    @Test(expected = ProductNotFoundException.class)
    public void whenProductIsNotFoundById() {
        productRepository.findById(10);
    }

    @Test
    public void whenAllProductsAreFound() {
        assertEquals(PRODUCTS, new HashSet<>(productRepository.findAll()));
    }

    @Test
    public void whenProductsAreFoundByTitle() {
        String title = "product1";
        assertEquals(new HashSet<>(Arrays.asList(PRODUCT1)), new HashSet<>(productRepository.findAllByParams("title", title, PRODUCT1.getOwner().getId(), false)));
    }

    @Test
    public void whenProductsAreNotFoundByTitle() {
        assertEquals(new HashSet<>(), new HashSet<>(productRepository.findAllByParams("title", "title for non-existing product", 1, true)));
    }

    @Test
    public void whenProductsAreFoundByDescription() {
        String desc = "desc for product1";
        assertEquals(new HashSet<>(Arrays.asList(PRODUCT1)), new HashSet<>(productRepository.findAllByParams("description", desc, PRODUCT1.getOwner().getId(), false)));
    }

    @Test
    public void whenProductsAreNotFoundByDescription() {
        assertEquals(new HashSet<>(), new HashSet<>(productRepository.findAllByParams("description", "desc for non-existing product", 1, true)));
    }

    @Test
    public void whenUserFoundHisProducts() {
        assertEquals(new HashSet<>(Arrays.asList(PRODUCT3, PRODUCT4)), new HashSet<>(productRepository.findAllByParams(null, null, USER3.getId(), false)));
    }

    @Test
    public void whenUserDidNotFindProducts() {
        assertEquals(new HashSet<>(), new HashSet<>(productRepository.findAllByParams(null, null, 10, false)));
    }

    @Test
    public void whenProductsAreFoundForUserAndOtherUsers() {
        assertEquals(PRODUCTS, new HashSet<>(productRepository.findAllByParams(null, null, USER1.getId(), true)));
    }

    @Test
    public void whenProductsAreFoundByStatus() {
        assertEquals(
                new HashSet<>(Arrays.asList(PRODUCT3, PRODUCT4)),
                new HashSet<>(productRepository.findAllByParams(ProductService.STATUS_PARAM, ProductStatus.FOR_SALE.toString(), null, true))
        );
    }

    @Rollback(true)
    @Test
    public void whenNewProductIsCreated() throws SQLException {
        productRepository.save(NEW_PRODUCT, USER1.getId());
        assertEquals(new HashSet<>(Arrays.asList(PRODUCT1, PRODUCT2, PRODUCT3, PRODUCT4, NEW_PRODUCT_AFTER_SAVE)), new HashSet<>(productRepository.findAll()));
    }

    @Rollback(true)
    @Test(expected = ProductNotFoundException.class)
    public void whenProductIsDeleted() {
        productRepository.delete(PRODUCT1.getId(), PRODUCT1.getOwner().getId());
        productRepository.findById(PRODUCT1.getId(), PRODUCT1.getOwner().getId());
    }
}
