package ru.demi;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ru.demi.controller.ControllerTestSuite;
import ru.demi.repository.RepositoryTestSuite;
import ru.demi.service.ServiceTestSuite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    RepositoryTestSuite.class,
    ServiceTestSuite.class,
    ControllerTestSuite.class
})
public class TestSuite {}
