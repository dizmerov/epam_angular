package ru.demi;

import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.USER2;
import static ru.demi.UserTestData.USER3;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.to.ProductTo;

public class ProductTestData {

    public static final int PRODUCT1_ID = 1;

    public static final Product PRODUCT1 = new ProductItem(PRODUCT1_ID, "product1", "desc for product1", 10000, 1000, 240, false, LocalDateTime.now().plusHours(240), ProductStatus.NOT_FOR_SALE, USER1);
    public static final Product PRODUCT2 = new ProductItem(PRODUCT1_ID + 1, "product2", "desc for product2", 12000, 2000, 120, false, LocalDateTime.now().plusHours(120), ProductStatus.NOT_FOR_SALE, USER2);
    public static final Product PRODUCT3 = new ProductItem(PRODUCT1_ID + 2, "product3", "product3 for sale", 10000, 0, 0, false, LocalDateTime.now().plusHours(0), ProductStatus.FOR_SALE, USER3);
    public static final Product PRODUCT4 = new ProductItem(PRODUCT1_ID + 3, "product4", "product4 for sale", 12000, 0, 0, false, LocalDateTime.now().plusHours(0), ProductStatus.FOR_SALE, USER3);

    public static final Set<Product> PRODUCTS = new HashSet<>(Arrays.asList(PRODUCT1, PRODUCT2, PRODUCT3, PRODUCT4));

    public static final Product PRODUCT_FOR_SAVE = new ProductItem(PRODUCT1, ProductStatus.FOR_SALE);
    public static final Product NEW_PRODUCT = new ProductItem(null, "new product", "desc for new product", 100000, 4000, 480, false, ProductStatus.NOT_FOR_SALE, USER1);
    public static final Product NEW_PRODUCT_AFTER_SAVE = new ProductItem(PRODUCT1_ID + 4, "new product", "desc for new product", 100000, 4000, 480, false, ProductStatus.NOT_FOR_SALE, USER1);

    public static final Matcher<ProductTo> MATCHER = new Matcher<>(ProductTo.class);

    public static final Function<Product, ProductTo> CONVERTER = product -> ((product instanceof ProductTo) ? (ProductTo) product : new ProductTo(product));
}