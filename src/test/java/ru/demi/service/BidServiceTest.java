package ru.demi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.demi.BidTestData.BID1;
import static ru.demi.BidTestData.BID2;
import static ru.demi.BidTestData.BID3;
import static ru.demi.BidTestData.NEW_BID;
import static ru.demi.BidTestData.NEW_BID_AFTER_SAVE;
import static ru.demi.ProductTestData.PRODUCT3;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ru.demi.exception.BidNotFoundException;
import ru.demi.exception.NotForSaleProductException;
import ru.demi.exception.NotValidArgumentException;
import ru.demi.exception.ProductNotFoundException;
import ru.demi.model.Bid;
import ru.demi.model.BidItem;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.model.Role;
import ru.demi.model.User;
import ru.demi.model.UserItem;
import ru.demi.repository.BidRepository;

@RunWith(JUnit4.class)
public class BidServiceTest {

    private BidRepository bidRepository;
    private BidService bidService;
    private ProductService productService;

    @Before
    public void setUp() {
        bidRepository = mock(BidRepository.class);
        productService = mock(ProductService.class);
        bidService = spy(new BidServiceImpl(bidRepository, productService));
    }

    @Test
    public void whenFindBidById() {
        int bidId = BID1.getId();
        int bidderId = BID1.getBidder().getId();
        when(bidRepository.findById(bidId, bidderId)).thenReturn(BID1);
        assertEquals(BID1, bidService.findById(bidId, bidderId));
        verify(bidRepository).findById(bidId, bidderId);
    }

    @Test
    public void whenFindBestBidByProductId() {
        int productId = BID2.getProduct().getId();
        LocalDateTime finishDate = BID2.getProduct().getFinishDate();
        when(bidRepository.findBestBid(productId, finishDate)).thenReturn(BID2);
        assertEquals(BID2, bidService.findBestBid(productId, finishDate));
        verify(bidRepository).findBestBid(productId, finishDate);
    }

    @Test
    public void whenFindBidsByProductId() {
        int productId = PRODUCT3.getId();
        List<Bid> bids = new ArrayList<>(Arrays.asList(BID1, BID2));
        when(bidRepository.findAllByProductId(productId)).thenReturn(bids);
        assertEquals(bids, bidService.findAllByProductId(productId));
        verify(bidRepository).findAllByProductId(productId);
    }

    @Test
    public void whenFindBidsByBidderId() {
        int bidderId = BID1.getBidder().getId();
        List<Bid> bids = new ArrayList<>(Arrays.asList(BID1, BID3));
        when(bidRepository.findAllByBidderId(bidderId)).thenReturn(bids);
        assertEquals(bids, bidService.findAllByBidderId(bidderId));
        verify(bidRepository).findAllByBidderId(bidderId);
    }

    @Test
    public void whenBidIsSaving() {
        when(bidRepository.save(NEW_BID)).thenReturn(NEW_BID_AFTER_SAVE);
        assertEquals(NEW_BID_AFTER_SAVE, bidService.save(NEW_BID));
        verify(bidRepository).save(NEW_BID);
    }

    @Test(expected = ProductNotFoundException.class)
    public void whenUserBidedForProductAndProductIdIsNotValid() {
        int userId = 1;
        int productId = 1;
        int offer = 700;

        doThrow(new ProductNotFoundException()).when(productService).findById(productId);

        bidService.bidForProduct(userId, productId, offer);
        verify(productService).findById(productId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTryToBidForProductThatNotForSale() {
        int bidderId = 2;
        int productId = 1;
        int offer = 1400;
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, ProductStatus.NOT_FOR_SALE, new UserItem(1));

        when(productService.findById(productId)).thenReturn(product);

        bidService.bidForProduct(bidderId, productId, offer);
        verify(productService).findById(productId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTryToBidHisProduct() {
        int bidderId = 2;
        int productId = 1;
        int offer = 1400;
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, ProductStatus.NOT_FOR_SALE, new UserItem(bidderId));

        when(productService.findById(productId)).thenReturn(product);

        bidService.bidForProduct(bidderId, productId, offer);
        verify(productService).findById(productId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTryToBidProductIsForBuyNow() {
        int bidderId = 2;
        int productId = 1;
        int offer = 1400;
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, true, ProductStatus.FOR_SALE, new UserItem(bidderId));

        when(productService.findById(productId)).thenReturn(product);

        bidService.bidForProduct(bidderId, productId, offer);
        verify(productService).findById(productId);
    }

    @Test
    public void whenUserBidedForProductAndWin() {
        int bidderId = 2;
        int productId = 1;
        int offer = 1500;
        User bidder = new UserItem(bidderId, "username", "billing address", "login", "pass", Role.ROLE_USER);
        LocalDateTime finishDate = LocalDateTime.now().plusHours(120);
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, finishDate, ProductStatus.FOR_SALE, new UserItem(1));
        Bid prevBestBid = new BidItem(2, new UserItem(3), 1200, product);
        Bid bidForSaving = new BidItem(null, new UserItem(bidderId), offer, new ProductItem(productId));
        Bid savedBid = new BidItem(1, bidder, offer, product);

        when(productService.findById(productId)).thenReturn(product);
        doReturn(prevBestBid).when(bidService).findBestBid(productId, finishDate);
        doReturn(savedBid).when(bidService).save(bidForSaving);

        assertEquals(savedBid, bidService.bidForProduct(bidderId, productId, offer));
        verify(productService).findById(productId);
        verify(bidService).findBestBid(productId, finishDate);
        verify(bidService).save(bidForSaving);
    }

    @Test
    public void whenUserBidedForProductAndLoseToPrevBid() {
        int bidderId = 2;
        int productId = 1;
        int offer = 700;
        LocalDateTime finishDate = LocalDateTime.now().plusHours(120);
        Product product = new ProductItem(1, "title", "description", 1000, 100, 120, false, finishDate, ProductStatus.FOR_SALE, new UserItem(1));
        Bid prevBestBid = new BidItem(2, new UserItem(3), 1200, product);

        when(productService.findById(productId)).thenReturn(product);
        doReturn(prevBestBid).when(bidService).findBestBid(productId, finishDate);

        assertNull(bidService.bidForProduct(bidderId, productId, offer));
        verify(productService).findById(productId);
        verify(bidService).findBestBid(productId, finishDate);
        // no saves
        verify(bidService, times(0)).save(any());
    }

    @Test
    public void whenUserBidedForProductAndLoseToStartPrice() {
        int bidderId = 2;
        int productId = 1;
        int offer = 700;
        LocalDateTime finishDate = LocalDateTime.now().plusHours(120);
        Product product = new ProductItem(1, "title", "description", 1000, 100, 120, false, finishDate, ProductStatus.FOR_SALE, new UserItem(1));

        when(productService.findById(productId)).thenReturn(product);
        doThrow(new BidNotFoundException()).when(bidService).findBestBid(productId, finishDate);

        assertNull(bidService.bidForProduct(bidderId, productId, offer));
        verify(productService).findById(productId);
        verify(bidService).findBestBid(productId, finishDate);
        // no saves
        verify(bidService, times(0)).save(any());
    }

    @Test
    public void whenBidsAreDeactivatedByProducts() {
        List<Integer> productIds = Arrays.asList(PRODUCT3.getId());
        List<Product> products = Arrays.asList(PRODUCT3);

        doNothing().when(bidRepository).deactivateBids(productIds);

        bidService.deactivateBids(products);

        verify(bidRepository).deactivateBids(productIds);
    }

    @Test(expected = NotValidArgumentException.class)
    public void whenBidsAreDeactivatedByProductsWithNotValidIds() {
        List<Product> products = Arrays.asList(new ProductItem(PRODUCT3, (Integer) null));

        bidService.deactivateBids(products);

        verify(bidRepository, never()).deactivateBids(any());
    }
}
