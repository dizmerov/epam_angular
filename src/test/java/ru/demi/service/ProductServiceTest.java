package ru.demi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.demi.ProductTestData.NEW_PRODUCT;
import static ru.demi.ProductTestData.NEW_PRODUCT_AFTER_SAVE;
import static ru.demi.ProductTestData.PRODUCT1;
import static ru.demi.ProductTestData.PRODUCT3;
import static ru.demi.ProductTestData.PRODUCT4;
import static ru.demi.ProductTestData.PRODUCTS;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ru.demi.exception.AccessDeniedException;
import ru.demi.exception.NotForSaleProductException;
import ru.demi.exception.NotValidArgumentException;
import ru.demi.exception.ProductNotFoundException;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.model.Role;
import ru.demi.model.User;
import ru.demi.model.UserItem;
import ru.demi.repository.ProductRepository;

@RunWith(JUnit4.class)
public class ProductServiceTest {

    private ProductRepository productRepository;
    private ProductService productService;
    private UserService userService;

    @Before
    public void setUp() {
        productRepository = mock(ProductRepository.class);
        userService = mock(UserService.class);
        productService = new ProductServiceImpl(productRepository, userService);
        productService = spy(productService);
    }

    @Test
    public void whenProductIsFoundById() {
        int productId = PRODUCT1.getId();
        when(productRepository.findById(productId)).thenReturn(PRODUCT1);
        assertEquals(PRODUCT1, productService.findById(productId));
        verify(productRepository).findById(productId);
    }

    @Test(expected = ProductNotFoundException.class)
    public void whenProductIsNotFoundById() {
        int productId = 105;
        doThrow(new ProductNotFoundException()).when(productRepository).findById(productId);
        productService.findById(productId);
        verify(productRepository).findById(productId);
    }

    @Test
    public void whenProductsAreFoundByTitle() {
        String title = "product";
        int ownerId = PRODUCT1.getOwner().getId();
        List<Product> products = Arrays.asList(PRODUCT1);
        when(productRepository.findAllByParams("title", title, ownerId, false)).thenReturn(products);
        assertEquals(products, productService.findAllByParams("title", title, ownerId, false));
        verify(productRepository).findAllByParams("title", title, ownerId, false);
    }

    @Test
    public void whenProductsAreNotFoundByTitle() {
        String title = "title";
        int ownerId = 1;
        List<Product> products = new ArrayList<>();
        when(productRepository.findAllByParams("title", title, ownerId, false)).thenReturn(products);
        assertEquals(products, productService.findAllByParams("title", title, ownerId, false));
        verify(productRepository).findAllByParams("title", title, ownerId, false);
    }

    @Test
    public void whenProductsAreFoundByDescription() {
        String description = "desc";
        int ownerId = PRODUCT1.getOwner().getId();
        List<Product> products = Arrays.asList(PRODUCT1);
        when(productRepository.findAllByParams("description", description, ownerId, false)).thenReturn(products);
        assertEquals(products, productService.findAllByParams("description", description, ownerId, false));
        verify(productRepository).findAllByParams("description", description, ownerId, false);
    }

    @Test
    public void whenProductsAreNotFoundByDescription() {
        String desc = "non-existent description";
        int ownerId = 1;
        List<Product> products = new ArrayList<>();
        when(productRepository.findAllByParams("description", desc, ownerId, false)).thenReturn(products);
        assertEquals(products, productService.findAllByParams("description", desc, ownerId, false));
        verify(productRepository).findAllByParams("description", desc, ownerId, false);
    }

    @Test
    public void whenUserFoundHisProducts() {
        int userId = PRODUCT3.getOwner().getId();
        List<Product> products = Arrays.asList(PRODUCT3, PRODUCT4);
        when(productRepository.findAllByParams(null, null, userId, false)).thenReturn(products);
        assertEquals(products, productService.findAllByParams(null, null, userId, false));
        verify(productRepository).findAllByParams(null, null, userId, false);
    }

    @Test
    public void whenUserFoundHisProductsAndOthers() {
        int userId = PRODUCT1.getOwner().getId();
        List<Product> products = new ArrayList<>(PRODUCTS);
        when(productRepository.findAllByParams(null, null, userId, true)).thenReturn(products);
        assertEquals(products, productService.findAllByParams(null, null, userId, true));
        verify(productRepository).findAllByParams(null, null, userId, true);
    }

    @Test
    public void whenUserDidNotFindAnyProducts() {
        int userId = 1;
        List<Product> products = new ArrayList<>();
        when(productRepository.findAllByParams(null, null, userId, true)).thenReturn(products);
        assertEquals(products, productService.findAllByParams(null, null, userId, true));
        verify(productRepository).findAllByParams(null, null, userId, true);
    }

    @Test(expected = NotValidArgumentException.class)
    public void whenFindAllByParamsIsCalledWithNotValidParams() {
        doThrow(NotValidArgumentException.class).when(productRepository).findAllByParams(null, null, null, false);
        productService.findAllByParams(null, null, null, false);
        verify(productRepository, never()).findAllByParams(null, null, null, false);
    }

    @Test
    public void whenProductIsSaved() {
        int ownerId = NEW_PRODUCT_AFTER_SAVE.getOwner().getId();
        when(productRepository.save(NEW_PRODUCT, ownerId)).thenReturn(NEW_PRODUCT_AFTER_SAVE);
        assertEquals(NEW_PRODUCT_AFTER_SAVE, productService.save(NEW_PRODUCT, ownerId));
        verify(productRepository).save(NEW_PRODUCT, ownerId);
    }

    @Test
    public void whenOwnerSoldProduct() {
        int ownerId = 1;
        int productId = 1;
        User user = new UserItem(ownerId, "username", "billing address", "login", "pass", Role.ROLE_USER);
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, ProductStatus.NOT_FOR_SALE, user);
        Product savedProduct = new ProductItem(productId, "title", "description", 1000, 100, 120, false, LocalDateTime.now().plusHours(120), ProductStatus.FOR_SALE, user);

        doReturn(product).when(productService).findById(productId, ownerId);
        doReturn(savedProduct).when(productService).save(product, user.getId());

        Product productForSale = productService.sellProduct(ownerId, productId);
        assertEquals(ProductStatus.FOR_SALE, productForSale.getStatus());
        assertNotNull(productForSale.getFinishDate());

        verify(productService).findById(productId, ownerId);
        verify(productService).save(savedProduct, user.getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void whenUserTriesToSellWithNotValidParams() {
        int userId = 100500;
        int productId = 10;

        doThrow(new ProductNotFoundException()).when(productService).findById(productId, userId);

        productService.sellProduct(userId, productId);
        verify(productService).findById(productId, userId);
        verify(productService, never()).save(any(), any());
    }

    @Test
    public void whenBidderBoughtProduct() {
        int ownerId = 1;
        int bidderId = 2;
        int productId = 1;
        User owner = new UserItem(ownerId, "owner", "billing address", "login", "pass", Role.ROLE_USER);
        User bidder = new UserItem(bidderId, "bidder", "address", "sologin", "sopass", Role.ROLE_USER);
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, true, ProductStatus.FOR_SALE, owner);
        Product savedProduct = new ProductItem(productId, "title", "description", 1000, 100, 120, true, ProductStatus.NOT_FOR_SALE, bidder);

        doReturn(product).when(productService).findById(productId);
        when(userService.findById(bidderId)).thenReturn(bidder);
        doReturn(savedProduct).when(productService).save(savedProduct, ownerId);

        assertEquals(savedProduct, productService.buyProduct(bidderId, productId, true));

        verify(productService).findById(productId);
        verify(userService).findById(bidderId);
        verify(productService).save(savedProduct, ownerId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTriesToBuyProductThatIsNotForBuyNow() {
        int ownerId = 1;
        int bidderId = 2;
        int productId = 1;
        User owner = new UserItem(ownerId, "owner", "billing address", "login", "pass", Role.ROLE_USER);
        User bidder = new UserItem(bidderId, "bidder", "address", "sologin", "sopass", Role.ROLE_USER);
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, ProductStatus.FOR_SALE, owner);

        doReturn(product).when(productService).findById(productId);
        when(userService.findById(bidderId)).thenReturn(bidder);

        productService.buyProduct(bidderId, productId, true);

        verify(productService).findById(productId);
        verify(userService).findById(bidderId);
        verify(productService, never()).save(any(), ownerId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTriesToBuyProductWithStatusNotForSale() {
        int ownerId = 1;
        int bidderId = 2;
        int productId = 1;
        User owner = new UserItem(ownerId, "owner", "billing address", "login", "pass", Role.ROLE_USER);
        User bidder = new UserItem(bidderId, "bidder", "address", "sologin", "sopass", Role.ROLE_USER);
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, ProductStatus.NOT_FOR_SALE, owner);

        doReturn(product).when(productService).findById(productId);
        when(userService.findById(bidderId)).thenReturn(bidder);

        productService.buyProduct(bidderId, productId, false);

        verify(productService).findById(productId);
        verify(userService).findById(bidderId);
        verify(productService, never()).save(any(), ownerId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTriesToBuyHisProduct() {
        int ownerId = 1;
        int bidderId = ownerId;
        int productId = 1;
        User owner = new UserItem(ownerId, "owner", "billing address", "login", "pass", Role.ROLE_USER);
        User bidder = owner;
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, ProductStatus.FOR_SALE, owner);

        doReturn(product).when(productService).findById(productId);
        when(userService.findById(bidderId)).thenReturn(bidder);

        productService.buyProduct(bidderId, productId, false);

        verify(productService).findById(productId);
        verify(userService).findById(bidderId);
        verify(productService, never()).save(any(), ownerId);
    }

    @Test(expected = NotForSaleProductException.class)
    public void whenUserTriesToBuyProductBeforeFinishDate() {
        int ownerId = 1;
        int bidderId = ownerId;
        int productId = 1;
        User owner = new UserItem(ownerId, "owner", "billing address", "login", "pass", Role.ROLE_USER);
        User bidder = owner;
        Product product = new ProductItem(productId, "title", "description", 1000, 100, 120, false, LocalDateTime.now().plusDays(1), ProductStatus.FOR_SALE, owner);

        doReturn(product).when(productService).findById(productId);
        when(userService.findById(bidderId)).thenReturn(bidder);

        productService.buyProduct(bidderId, productId, false);

        verify(productService).findById(productId);
        verify(userService).findById(bidderId);
        verify(productService, never()).save(any(), ownerId);
    }
}
