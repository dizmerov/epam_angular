package ru.demi.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    BidServiceTest.class,
    ProductServiceTest.class,
    UserServiceTest.class
})
public class ServiceTestSuite {}