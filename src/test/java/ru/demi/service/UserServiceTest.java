package ru.demi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.demi.UserTestData.NEW_USER;
import static ru.demi.UserTestData.NEW_USER_AFTER_SAVE;
import static ru.demi.UserTestData.USER1;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ru.demi.exception.NotValidArgumentException;
import ru.demi.exception.UserNotFoundException;
import ru.demi.repository.UserRepository;

@RunWith(JUnit4.class)
public class UserServiceTest {

    private UserRepository userRepository;
    private UserService userService;

    @Before
    public void setUp() {
        userRepository = mock(UserRepository.class);
        userService = spy(new UserServiceImpl(userRepository));
    }

    @Test
    public void whenUserIsFoundById() {
        int userId = USER1.getId();
        when(userRepository.findById(userId)).thenReturn(USER1);
        assertEquals(USER1, userService.findById(userId));
        verify(userRepository).findById(userId);
    }

    @Test
    public void whenUserIsFoundByLogin() {
        when(userRepository.findByLogin(USER1.getLogin())).thenReturn(USER1);
        assertEquals(USER1, userService.findByLogin(USER1.getLogin()));
        verify(userRepository).findByLogin(USER1.getLogin());
    }

    @Test
    public void whenFindUserByLoginAndPassword() {
        when(userRepository.findByLoginAndPassword(USER1.getLogin(), USER1.getPassword())).thenReturn(USER1);
        assertEquals(USER1, userService.findByLoginAndPassword(USER1.getLogin(), USER1.getPassword()));
        verify(userRepository).findByLoginAndPassword(USER1.getLogin(), USER1.getPassword());
    }

    @Test(expected = NotValidArgumentException.class)
    public void whenTryFindUserByLoginAndPasswordAndAndLoginIsMissed() {
        userService.findByLoginAndPassword(null, "password");
    }

    @Test(expected = NotValidArgumentException.class)
    public void whenUserLogInsAndPasswordIsMissed() {
        userService.findByLoginAndPassword("login", null);
    }

    @Test
    public void whenUserIsSaved() {
        when(userRepository.save(NEW_USER)).thenReturn(NEW_USER_AFTER_SAVE);
        assertEquals(NEW_USER_AFTER_SAVE, userService.save(NEW_USER));
        verify(userRepository).save(NEW_USER);
    }

    @Test
    public void whenUserExists() {
        String login = USER1.getLogin();
        when(userRepository.findByLogin(login)).thenReturn(USER1);

        assertTrue(userService.exists(login));

        verify(userRepository).findByLogin(login);
    }

    @Test
    public void whenUserDoesNotExist() {
        String login = "non-existent login";
        doThrow(new UserNotFoundException()).when(userRepository).findByLogin(login);

        assertFalse(userService.exists(login));

        verify(userRepository).findByLogin(login);
    }
}
