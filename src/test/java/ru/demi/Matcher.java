package ru.demi;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Matcher<T> {

    private Class<T> entityClass;

    public Matcher(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public ResultMatcher contentMatcher(T expected, ObjectMapper objectMapper) {
        return new ResultMatcher() {
            @Override
            public void match(MvcResult result) throws Exception {
                String content = result.getResponse().getContentAsString();
                T actual = objectMapper.readValue(content, entityClass);
                Assert.assertEquals(expected, actual);
            }
        };
    }

    public ResultMatcher contentSetMatcher(ObjectMapper objectMapper, T... expected) {
        return contentSetMatcher(new HashSet<T>(Arrays.asList(expected)), objectMapper);
    }

    public ResultMatcher contentSetMatcher(Set<T> expected, ObjectMapper objectMapper) {
        return new ResultMatcher() {
            @Override
            public void match(MvcResult result) throws Exception {
                String content = result.getResponse().getContentAsString();
                Set<T> actual = new HashSet<T>(objectMapper.readValue(content, objectMapper.getTypeFactory().constructCollectionType(List.class, entityClass)));
                Assert.assertTrue(expected.equals(actual));
            }
        };
    }

    public static <S, T> Set<T> map(Collection<S> collection, Function<S, T> converter) {
        return collection.stream().map(converter).collect(Collectors.toSet());
    }
}
