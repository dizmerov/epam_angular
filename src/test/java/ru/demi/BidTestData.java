package ru.demi;

import static ru.demi.ProductTestData.PRODUCT3;
import static ru.demi.ProductTestData.PRODUCT4;
import static ru.demi.UserTestData.USER1;
import static ru.demi.UserTestData.USER2;
import static ru.demi.UserTestData.USER4;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import ru.demi.model.Bid;
import ru.demi.model.BidItem;
import ru.demi.to.BidTo;

public class BidTestData {

    public static final int BID1_ID = 1;

    public static final Bid BID1 = new BidItem(BID1_ID, USER1, 11000, PRODUCT3, LocalDateTime.now().minusDays(1), true);
    public static final Bid BID2 = new BidItem(BID1_ID + 1, USER2, 12000, PRODUCT3, LocalDateTime.now().minusDays(2), true);
    public static final Bid BID3 = new BidItem(BID1_ID + 2, USER1, 15000, PRODUCT4, LocalDateTime.now().minusDays(3), true);
    public static final Bid BID4 = new BidItem(BID1_ID + 3, USER2, 14000, PRODUCT4, LocalDateTime.now().minusDays(4), true);

    public static final Bid NEW_BID = new BidItem(null, USER4, 14000, PRODUCT3, LocalDateTime.now(), true);
    public static final Bid NEW_BID_AFTER_SAVE = new BidItem(BID1_ID + 4, USER4, 14000, PRODUCT3, LocalDateTime.now(), true);

    public static final Set<Bid> BIDS = new HashSet<>(Arrays.asList(BID1, BID2, BID3, BID4));

    public static final Matcher<BidTo> MATCHER = new Matcher<BidTo>(BidTo.class);

    public static final Function<Bid, BidTo> CONVERTER = bid -> ((bid instanceof BidTo) ? (BidTo) bid : new BidTo(bid));
}
