package ru.demi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import ru.demi.model.Role;
import ru.demi.model.User;
import ru.demi.model.UserItem;
import ru.demi.to.LoggedUser;
import ru.demi.to.UserTo;

public class UserTestData {

    public static final int USER1_ID = 1;
    public static final int ADMIN_ID = USER1_ID + 4;

    public static final User USER1 = new UserItem(USER1_ID, "login1", "password1", Role.ROLE_USER);
    public static final User USER2 = new UserItem(USER1_ID + 1, "login2", "password2", Role.ROLE_USER);
    public static final User USER3 = new UserItem(USER1_ID + 2, "login3", "password3", Role.ROLE_USER);
    public static final User USER4 = new UserItem(USER1_ID + 3, "login4", "password4", Role.ROLE_USER);
    public static final User ADMIN = new UserItem(ADMIN_ID, "admin", "admin", Role.ROLE_ADMIN);

    public static final User GUEST = UserItem.GUEST;
    public static final User USER_FOR_SAVE = new UserItem(USER1, "pass");
    public static final User NEW_USER = new UserItem(null, "user5", "bill address5", "login5", "password5", Role.ROLE_USER);
    public static final User NEW_USER_AFTER_SAVE = new UserItem(USER1_ID + 5, "user5", "bill address5", "login5", "password5", Role.ROLE_USER);

    public static final Set<User> USERS = new HashSet<>(Arrays.asList(USER1, USER2, USER3, USER4, ADMIN));

    public static final Matcher<UserItem> MATCHER = new Matcher<UserItem>(UserItem.class);

    public static final Function<User, UserItem> CONVERTER = user -> ((user instanceof UserItem) ? (UserItem) user : new UserItem(user));


    public static final Matcher<UserTo> MATCHER_TO = new Matcher<UserTo>(UserTo.class);

    public static final Function<User, UserTo> CONVERTER_TO = user -> ((user instanceof UserTo) ? (UserTo) user : new UserTo(user));

    public static LoggedUser toLoggedUser(User user) {
        return new LoggedUser(user);
    }
}
