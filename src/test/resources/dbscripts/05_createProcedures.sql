-- CREATE PROCEDURES
CREATE OR REPLACE PROCEDURE resetSequence (seqName IN VARCHAR2, startValue IN PLS_INTEGER) 
AS
    v_val NUMBER;
    v_incBy VARCHAR2(25);
    v_prevInc NUMBER := 1;
BEGIN
    SELECT INCREMENT_BY 
        INTO v_prevInc
        FROM USER_SEQUENCES 
        WHERE SEQUENCE_NAME = UPPER(seqName);
        
    EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || seqName || ' MINVALUE 0';
    EXECUTE IMMEDIATE 'SELECT ' || seqName || '.NEXTVAL FROM DUAL' INTO v_val;
    v_val := v_val - startValue + 1;
    IF v_val < 0 THEN
        v_incBy := ' INCREMENT BY ';
        v_val:= ABS(v_val);
    ELSE
        v_incBy := ' INCREMENT BY -';
    END IF;
    EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || seqName || v_incBy || v_val;
    EXECUTE IMMEDIATE 'SELECT ' || seqName || '.NEXTVAL FROM DUAL' INTO v_val;
    EXECUTE IMMEDIATE 'ALTER SEQUENCE ' || seqName || ' INCREMENT BY ' || v_prevInc;
END;
/
