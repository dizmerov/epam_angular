package ru.demi.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;

import com.fasterxml.jackson.annotation.JsonFormat;

@DynamicInsert
@Entity
@Table(name = "products")
public class ProductItem implements Product, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seqProducts", sequenceName = "seqProducts", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seqProducts")
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "title", nullable = false, length = 20)
    private String title;

    @Column(name = "description", nullable = false, length = 100)
    private String description;

    @Column(name = "startPrice", nullable = false, length = 20)
    private long startPrice;

    @Column(name = "bidStep", nullable = true, length = 10)
    private Integer bidStep;

    @Column(name = "hoursToEnd", nullable = true, length = 10)
    private Integer hoursToEnd;

    @Column(name = "isBuyNow", nullable = false, length = 1)
    private boolean isBuyNow;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "finishDate", nullable = true)
    private LocalDateTime finishDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = true, columnDefinition = "VARCHAR2(20) DEFAULT 'NOT_FOR_SALE'")
    private ProductStatus status;

    @ManyToOne(targetEntity = UserItem.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "ownerId", nullable = false)
    private User owner;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, targetEntity = BidItem.class)
    private List<Bid> bids;

    public ProductItem() {}

    public ProductItem(Integer id, String title, String description, long startPrice, Integer bidStep, Integer hoursToEnd,
            boolean isBuyNow, LocalDateTime finishDate, ProductStatus status, User owner) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.startPrice = startPrice;
        this.bidStep = bidStep;
        this.hoursToEnd = hoursToEnd;
        this.isBuyNow = isBuyNow;
        this.finishDate = finishDate;
        this.status = status;
        this.owner = owner;
    }

    public ProductItem(Integer id, String title, String description, long startPrice, Integer bidStep, Integer hoursToEnd,
            boolean isBuyNow, ProductStatus status, User owner) {
        this(id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, null, status, owner);
    }

    public ProductItem(Product product) {
        this(product.getId(), product.getTitle(), product.getDescription(), product.getStartPrice(), product.getBidStep(), product.getHoursToEnd(),
                product.isBuyNow(), product.getFinishDate(), product.getStatus(), product.getOwner());
    }

    public ProductItem(Product product, Integer id, LocalDateTime finishDate, ProductStatus status, User owner) {
        this(id, product.getTitle(), product.getDescription(), product.getStartPrice(), product.getBidStep(), product.getHoursToEnd(),
                product.isBuyNow(), finishDate, status, owner);
    }

    public ProductItem(Product product, ProductStatus status, User owner) {
        this(product, product.getId(), product.getFinishDate(), status, owner);
    }

    public ProductItem(Product product, LocalDateTime finishDate, ProductStatus status) {
        this(product, product.getId(), finishDate, status, product.getOwner());
    }

    public ProductItem(Product product, ProductStatus status) {
        this(product, product.getId(), product.getFinishDate(), status, product.getOwner());
    }

    public ProductItem(Product product, Integer id) {
        this(product, id, product.getFinishDate(), product.getStatus(), product.getOwner());
    }

    public ProductItem(Product product, User owner) {
        this(product, product.getId(), product.getFinishDate(), product.getStatus(), owner);
    }

    public ProductItem(int id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setUID(Integer id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public long getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(long startPrice) {
        this.startPrice = startPrice;
    }

    @Override
    public Integer getBidStep() {
        return bidStep;
    }

    public void setBidStep(Integer bidStep) {
        this.bidStep = bidStep;
    }

    @Override
    public Integer getHoursToEnd() {
        return hoursToEnd;
    }

    public void setHoursToEnd(Integer hoursToEnd) {
        this.hoursToEnd = hoursToEnd;
    }

    @Override
    public boolean isBuyNow() {
        return isBuyNow;
    }

    public void setBuyNow(boolean isBuyNow) {
        this.isBuyNow = isBuyNow;
    }

    @Override
    public LocalDateTime getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
        this.finishDate = finishDate;
    }

    @Override
    public ProductStatus getStatus() {
        return status;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Override
    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ProductItem))
            return false;
        ProductItem other = (ProductItem) obj;
        if (getId() != other.getId())
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;
        if (getTitle() == null) {
            if (other.getTitle() != null)
                return false;
        } else if (!getTitle().equals(other.getTitle()))
            return false;
        return true;
    }
}
