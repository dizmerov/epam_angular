package ru.demi.model;

import java.time.LocalDateTime;
import java.util.List;

public interface Product {
    Integer getId();

    String getTitle();

    String getDescription();

    long getStartPrice();

    Integer getBidStep();

    Integer getHoursToEnd();

    boolean isBuyNow();

    LocalDateTime getFinishDate();

    ProductStatus getStatus();

    User getOwner();

    List<Bid> getBids();
}
