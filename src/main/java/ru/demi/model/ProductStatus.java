package ru.demi.model;

public enum ProductStatus {
    FOR_SALE,
    NOT_FOR_SALE
}
