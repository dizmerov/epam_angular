package ru.demi.model;

public interface User {
    Integer getId();

    String getName();

    String getBillingAddress();

    String getLogin();

    String getPassword();

    Role getRole();
}
