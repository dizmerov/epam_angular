package ru.demi.model;

import java.time.LocalDateTime;

public interface Bid {
    Integer getId();

    User getBidder();

    long getOffer();

    Product getProduct();

    LocalDateTime getOfferDate();

    boolean isActive();
}
