package ru.demi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicInsert;

@DynamicInsert
@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = {"login"}))
public class UserItem implements User, Serializable {

    private static final long serialVersionUID = 1L;

    public static final User GUEST = new UserItem(null, "Guest", null, null, null, Role.ROLE_GUEST);

    @Id
    @SequenceGenerator(name = "seqUsers", sequenceName = "seqUsers", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seqUsers")
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 20)
    private String name;

    @Column(name = "billingAddress", nullable = false, length = 50)
    private String billingAddress;

    @Column(name = "login", unique = true, nullable = false, length = 20)
    private String login;

    @Column(name = "password", nullable = false, length = 20)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name="role", columnDefinition="VARCHAR2(20) DEFAULT 'ROLE_USER'")
    private Role role;

    public UserItem() {}

    public UserItem(Integer id, String name, String billingAddress, String login, String password, Role role) {
        this.id = id;
        this.name = name;
        this.billingAddress = billingAddress;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public UserItem(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public UserItem(Integer id, String login, String password, Role role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public UserItem(User user) {
        this(user.getId(), user.getName(), user.getBillingAddress(), user.getLogin(), user.getPassword(), user.getRole());
    }

    public UserItem(User user, String password) {
        this(user.getId(), user.getName(), user.getBillingAddress(), user.getLogin(), password, user.getRole());
    }

    public UserItem(User user, int id) {
        this(id, user.getName(), user.getBillingAddress(), user.getLogin(), user.getPassword(), user.getRole());
    }

    public UserItem(int id) {
        this.id = id;
    }

    public UserItem(User user, Role role) {
        this(user);
        setRole(role);
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    @Override
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id != null ? id.hashCode() : 0);
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof UserItem))
            return false;
        UserItem other = (UserItem) obj;
        Integer gotId = other.getId();
        if (getId() != gotId)
            return false;
        if (getLogin() == null) {
            if (other.getLogin() != null)
                return false;
        } else if (!getLogin().equals(other.getLogin()))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "UserItem [id=" + id + ", login=" + login + "]";
    }
}
