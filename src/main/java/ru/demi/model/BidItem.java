package ru.demi.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "bids")
public class BidItem implements Bid, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seqBids", sequenceName = "seqBids", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqBids")
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(targetEntity = UserItem.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "bidderId", nullable = false)
    private User bidder;

    @Column(name = "offer", nullable = false, length = 20)
    private long offer;

    @ManyToOne(targetEntity = ProductItem.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name = "offerDate", nullable = true)
    private LocalDateTime offerDate;

    @Column(name = "isActive", nullable = false, length = 1)
    private boolean isActive;

    public BidItem() {}

    public BidItem(Integer id, User bidder, long offer, Product product, LocalDateTime offerDate, boolean isActive) {
        this.id = id;
        this.bidder = bidder;
        this.offer = offer;
        this.product = product;
        this.offerDate = offerDate;
        this.isActive = isActive;
    }

    public BidItem(Integer id, User bidder, long offer, Product product) {
        this(id, bidder, offer, product, null, true);
    }


    public BidItem(Bid bid) {
        this(bid.getId(), bid.getBidder(), bid.getOffer(), bid.getProduct(), bid.getOfferDate(), bid.isActive());
    }

    public BidItem(Bid bid, int id) {
        this(id, bid.getBidder(), bid.getOffer(), bid.getProduct(), bid.getOfferDate(), bid.isActive());
    }

    public BidItem(Bid bid, User user) {
        this(bid.getId(), user, bid.getOffer(), bid.getProduct(), bid.getOfferDate(), bid.isActive());
    }

    public BidItem(Bid bid, boolean isActive) {
        this(bid.getId(), bid.getBidder(), bid.getOffer(), bid.getProduct(), bid.getOfferDate(), isActive);
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public User getBidder() {
        return bidder;
    }

    public void setBidder(User bidder) {
        this.bidder = bidder;
    }

    @Override
    public long getOffer() {
        return offer;
    }

    public void setOffer(long offer) {
        this.offer = offer;
    }

    @Override
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public LocalDateTime getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(LocalDateTime offerDate) {
        this.offerDate = offerDate;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bidder == null) ? 0 : bidder.getId().hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((product == null) ? 0 : product.getId().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BidItem other = (BidItem) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (id.equals(other.id))
            return true;
        if (bidder == null) {
            if (other.bidder != null)
                return false;
        } else if (!bidder.equals(other.bidder))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        return offer == other.getOffer();
    }
}
