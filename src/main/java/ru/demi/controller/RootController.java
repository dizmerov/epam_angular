package ru.demi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ru.demi.form.RegisterFormItem;
import ru.demi.model.UserItem;
import ru.demi.service.UserService;
import ru.demi.util.JSONResponse;

@RestController
public class RootController {

    @Autowired
    private UserService service;

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JSONResponse> register(@RequestBody @Valid RegisterFormItem registerItem, BindingResult result) {
        if (!result.hasErrors()) {
            service.save(new UserItem(registerItem.toUser()));
            return new ResponseEntity<>(new JSONResponse(HttpStatus.OK.getReasonPhrase()), HttpStatus.OK);
        }
        return new ResponseEntity<>(new JSONResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), result), HttpStatus.BAD_REQUEST);
    }
}
