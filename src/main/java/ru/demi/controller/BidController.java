package ru.demi.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.demi.model.Bid;
import ru.demi.service.BidService;
import ru.demi.to.BidTo;
import ru.demi.to.LoggedUser;

@RestController
@RequestMapping(value = BidController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class BidController {

    public static final String REST_URL = "/bids";

    @Autowired
    private BidService bidService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Bid getById(@AuthenticationPrincipal LoggedUser user, @PathVariable("id") int id)  {
        return BidTo.from(bidService.findById(id, user.getId()));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Bid> getAll(@AuthenticationPrincipal LoggedUser user) {
        return bidService.findAllByBidderId(user.getId()).stream().map(BidTo::from).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBid(@AuthenticationPrincipal LoggedUser user, @PathVariable("id") int id) {
        bidService.delete(id, user.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/products/{productId}", method = RequestMethod.POST)
    public Bid bidForProduct(@AuthenticationPrincipal LoggedUser user, @PathVariable("productId") int productId, @RequestParam("offer") long offer) {
        Bid bidForProduct = bidService.bidForProduct(user.getId(), productId, offer);
        return BidTo.from(bidForProduct);
    }
}
