package ru.demi.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.demi.form.ProductFormItem;
import ru.demi.model.Product;
import ru.demi.service.ProductService;
import ru.demi.to.LoggedUser;
import ru.demi.to.ProductTo;
import ru.demi.util.JSONResponse;

@RestController
@RequestMapping(value = ProductController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    public static final String REST_URL = "/products";

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Product getById(@AuthenticationPrincipal LoggedUser user, @PathVariable("id") int id) {
        return ProductTo.from(productService.findById(id, user.getId()));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Product> getAll(@AuthenticationPrincipal LoggedUser user, @RequestParam(value = "id", defaultValue = "") String id,
                                @RequestParam(value = "title", defaultValue = "") String title, @RequestParam(value = "desc", defaultValue = "") String desc,
                                @RequestParam(value = "withOthers", defaultValue = "") String withOthers) {
        boolean wOthers = !withOthers.isEmpty();
        List<Product> products = null;
        if (id != null && !id.isEmpty()) {
            products = productService.findAllByParams(ProductService.ID_PARAM, id, user.getId(), wOthers);
        } else if (title != null && !title.isEmpty()) {
            products = productService.findAllByParams(ProductService.TITLE_PARAM, title, user.getId(), wOthers);
        } else if (desc != null && !desc.isEmpty()) {
            products = productService.findAllByParams(ProductService.DESC_PARAM, desc, user.getId(), wOthers);
        } else {
            products = productService.findAllByParams(null, null, user.getId(), wOthers);
        }
        return products.stream().map(ProductTo::from).collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JSONResponse> updateProduct(@AuthenticationPrincipal LoggedUser user, @RequestBody @Valid ProductFormItem productItem, BindingResult result) {
        if (!result.hasErrors()) {
            productService.save(productItem.toProduct(), user.getId());
            return new ResponseEntity<>(new JSONResponse(HttpStatus.OK.getReasonPhrase()), HttpStatus.OK);
        }
        return new ResponseEntity<>(new JSONResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JSONResponse> addProduct(@AuthenticationPrincipal LoggedUser user, @RequestBody @Valid ProductFormItem productItem, BindingResult result) {
        if (!result.hasErrors()) {
            productService.save(productItem.toProduct(), user.getId());
            return new ResponseEntity<>(new JSONResponse(HttpStatus.CREATED.getReasonPhrase()), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new JSONResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@AuthenticationPrincipal LoggedUser user, @PathVariable("id") int id) {
        productService.delete(id, user.getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}/sell", method = RequestMethod.POST)
    public ResponseEntity<?> sellProduct(@AuthenticationPrincipal LoggedUser user, @PathVariable("productId") int productId) {
        productService.sellProduct(user.getId(), productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}/buy", method = RequestMethod.POST)
    public ResponseEntity<?> buyProductNow(@AuthenticationPrincipal LoggedUser user, @PathVariable("productId") int productId) {
        productService.buyProduct(user.getId(), productId, false);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
