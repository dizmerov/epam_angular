package ru.demi.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ru.demi.model.Product;
import ru.demi.model.ProductItem;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductFormItem {

    private Integer id;

    @NotNull
    @Size(min=1, max=20)
    private String title;

    @NotNull
    @Size(min=1, max=100)
    private String description;

    @Min(1)
    private long startPrice;

    private Integer bidStep = 0;

    private Integer hoursToEnd = 0;

    private boolean isBuyNow = false;

    public Integer getId() {
        return id;
    }

    public void setUID(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(long startPrice) {
        this.startPrice = startPrice;
    }

    public Integer getBidStep() {
        return bidStep;
    }

    public void setBidStep(Integer bidStep) {
        this.bidStep = bidStep;
    }

    public Integer getHoursToEnd() {
        return hoursToEnd;
    }

    public void setHoursToEnd(Integer hoursToEnd) {
        this.hoursToEnd = hoursToEnd;
    }

    public boolean isBuyNow() {
        return isBuyNow;
    }

    public void setBuyNow(boolean isBuyNow) {
        this.isBuyNow = isBuyNow;
    }

    public Product toProduct() {
        return new ProductItem(id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, null, null, null);
    }
}
