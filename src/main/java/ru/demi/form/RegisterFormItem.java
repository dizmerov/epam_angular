package ru.demi.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ru.demi.model.User;
import ru.demi.model.UserItem;

public class RegisterFormItem {

    @NotNull
    @Size(min=1, max=20)
    private String name;

    @NotNull
    @Size(min=1, max=50)
    private String billingAddress;

    @NotNull
    @Size(min=1, max=20)
    private String login;

    @NotNull
    @Size(min=6, max=20)
    private String password;

    @NotNull
    @Size(min=6, max=20)
    private String repeatPassword;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public User toUser() {
        return new UserItem(null, name, billingAddress, login, password, null);
    }
}
