package ru.demi.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class JSONResponse {

    private Map<String, Object> data;
    private Map<String, String> errors;

    public JSONResponse(){}

    public JSONResponse(Object object) {
        this.data = new HashMap<>();
        this.data.put("message", object);
    }

    public JSONResponse(String message) {
        this.data = new HashMap<>();
        this.data.put("message", message);
    }

    public JSONResponse(BindingResult result) {
        errors = handleErrors(result);
    }

    public JSONResponse(Object object, BindingResult result) {
        this.data = new HashMap<>();
        this.data.put("message", object);
        errors = handleErrors(result);
    }

    public JSONResponse(String message, BindingResult result) {
        this.data = new HashMap<>();
        this.data.put("message", message);
        errors = handleErrors(result);
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    private Map<String, String> handleErrors(BindingResult result) {
        Map<String, String> map = new HashMap<>();
        for (Object object : result.getAllErrors()) {
            if(object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                map.put(fieldError.getField(), fieldError.getDefaultMessage());
            }

            if(object instanceof ObjectError) {
                ObjectError objectError = (ObjectError) object;
                map.put(objectError.getObjectName(), objectError.getDefaultMessage());
            }
        }
        return map;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        result = prime * result + ((errors == null) ? 0 : errors.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JSONResponse other = (JSONResponse) obj;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        if (errors == null) {
            if (other.errors != null)
                return false;
        } else if (!errors.equals(other.errors))
            return false;
        return true;
    }
}
