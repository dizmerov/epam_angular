package ru.demi.util;

import org.springframework.jdbc.core.JdbcTemplate;

public class DBHelper {

    private DBHelper() {}

    public static Integer getNextId(JdbcTemplate jdbcTemplate, String seqName) {
        return jdbcTemplate.queryForObject("SELECT " + seqName + ".NEXTVAL FROM DUAL", new Object[] {}, Integer.class);
    }
}