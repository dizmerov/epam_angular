package ru.demi.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ru.demi.controller.BidController;
import ru.demi.exception.NotValidArgumentException;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidController.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NotValidArgumentException.class)
    @ResponseBody
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public JSONResponse handleNoValidArgumentException(HttpServletRequest req, Exception e) {
        LOGGER.warn("", e);
        return new JSONResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    @Order(Ordered.LOWEST_PRECEDENCE)
    public JSONResponse handleError(HttpServletRequest req, Exception e) {
        LOGGER.warn("", e);
        return new JSONResponse(HttpStatus.NOT_FOUND.getReasonPhrase());
    }
}
