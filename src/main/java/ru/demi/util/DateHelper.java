package ru.demi.util;

import java.time.format.DateTimeFormatter;

public class DateHelper {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
}
