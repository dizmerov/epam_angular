package ru.demi.exception;

public class DBException extends RuntimeException {

    public DBException(Throwable cause) {
        super(cause);
    }

    public DBException(String string) {
        super(string);
    }
}
