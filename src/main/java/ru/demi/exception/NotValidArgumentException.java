package ru.demi.exception;

public class NotValidArgumentException extends ServiceException {

    public NotValidArgumentException() {
        super();
    }

    public NotValidArgumentException(String message) {
        super(message);
    }
}
