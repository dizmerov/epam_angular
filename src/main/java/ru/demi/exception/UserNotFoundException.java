package ru.demi.exception;

public class UserNotFoundException extends RepositoryException {

    public UserNotFoundException(Throwable e) {
        super(e);
    }

    public UserNotFoundException() {}
}
