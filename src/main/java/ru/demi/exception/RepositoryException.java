package ru.demi.exception;

public class RepositoryException extends RuntimeException {

    public RepositoryException() {}

    public RepositoryException(Throwable e) {
        super(e);
    }
}
