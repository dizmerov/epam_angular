package ru.demi.task;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ru.demi.model.Bid;
import ru.demi.model.Product;
import ru.demi.model.ProductStatus;
import ru.demi.service.BidService;
import ru.demi.service.ProductService;

@Component
public class ProductTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductTask.class);

    @Autowired
    ProductService productService;

    @Autowired
    BidService bidService;

    @Scheduled(cron = "0 0 * * * *")
    public void buyProducts() {
        LOGGER.debug("ProductTask::buyProducts starts");
        List<Product> products = productService.findAllByParams(ProductService.STATUS_PARAM, ProductStatus.FOR_SALE.toString(), null, true);
        LocalDateTime now = LocalDateTime.now();
        products = products.stream().filter(product -> {

            LocalDateTime finishDate = product.getFinishDate();
            if (finishDate == null || product.isBuyNow()) {
                return false;
            }

            return finishDate.isBefore(now);

        }).collect(Collectors.toList());

        products.forEach(product -> {
            Bid bestBid = bidService.findBestBid(product.getId(), product.getFinishDate());
            productService.buyProduct(bestBid.getBidder().getId(), product.getId(), false);
        });

        bidService.deactivateBids(products);

        LOGGER.debug("ProductTask::buyProducts ends");
    }
}
