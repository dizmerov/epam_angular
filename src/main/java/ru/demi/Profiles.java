package ru.demi;

public class Profiles {
    public static final String
            ORACLE = "oracle",
            HSQLDB = "hsqldb",
            JDBC = "jdbc",
            JPA = "jpa";
}