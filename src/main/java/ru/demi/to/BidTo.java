package ru.demi.to;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ru.demi.model.Bid;
import ru.demi.model.Product;
import ru.demi.model.User;

public class BidTo implements Bid {

    private Integer id;

    @JsonDeserialize(as=UserTo.class)
    private User bidder;

    private long offer;

    public BidTo(){}

    public BidTo(Integer id, User bidder, long offer) {
        this.id = id;
        this.bidder = new UserTo(bidder);
        this.offer = offer;
    }

    public BidTo(Bid bid) {
        this(bid.getId(), bid.getBidder(), bid.getOffer());
    }

    public BidTo(Bid bid, User user) {
        this(bid.getId(), user, bid.getOffer());
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public User getBidder() {
        return bidder;
    }

    public void setBidder(User bidder) {
        this.bidder = bidder;
    }

    @Override
    public long getOffer() {
        return offer;
    }

    public void setOffer(long offer) {
        this.offer = offer;
    }

    @Override
    public Product getProduct() {
        return null;
    }

    @Override
    public LocalDateTime getOfferDate() {
        return null;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    public static Bid from(Bid bid) {
        return new BidTo(bid);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bidder == null) ? 0 : bidder.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + (int) (offer ^ (offer >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BidTo other = (BidTo) obj;
        if (bidder == null) {
            if (other.bidder != null)
                return false;
        } else if (!bidder.equals(other.bidder))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (offer != other.offer)
            return false;
        return true;
    }
}
