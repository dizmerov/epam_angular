package ru.demi.to;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ru.demi.model.Bid;
import ru.demi.model.Product;
import ru.demi.model.ProductStatus;
import ru.demi.model.User;

public class ProductTo implements Product {

    private Integer id;

    private String title;

    private String description;

    private long startPrice;

    private Integer bidStep;

    private Integer hoursToEnd;

    private boolean isBuyNow;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime finishDate;

    private ProductStatus status;

    @JsonDeserialize(as = UserTo.class)
    private User owner;

    @JsonDeserialize(as = BidTo.class)
    private Bid bestBid;

    public ProductTo(){}

    public ProductTo(Product product) {
        this.id = product.getId();
        this.title = product.getTitle();
        this.description = product.getDescription();
        this.startPrice = product.getStartPrice();
        this.bidStep = product.getBidStep();
        this.hoursToEnd = product.getHoursToEnd();
        this.isBuyNow = product.isBuyNow();
        this.finishDate = product.getFinishDate();
        this.status = product.getStatus();
        this.owner = new UserTo(product.getOwner());

        Bid bestBid = (product.getBids() != null && product.getBids().size() > 0) ?
                product.getBids().stream().sorted(new Comparator<Bid>() {
                    @Override
                    public int compare(Bid o1, Bid o2) {
                        if (o2.getOffer() > o1.getOffer()) {
                            return 1;
                        } else if (o1.getOffer() > o2.getOffer()) {
                            return -1;
                        }
                        return 0;
                    }
                }).limit(1).collect(Collectors.toList()).get(0) : null;

        this.bestBid = (bestBid == null) ? null : new BidTo(bestBid, new UserTo(bestBid.getBidder()));
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public long getStartPrice() {
        return startPrice;
    }

    @Override
    public Integer getBidStep() {
        return bidStep;
    }

    @Override
    public Integer getHoursToEnd() {
        return hoursToEnd;
    }

    @Override
    public boolean isBuyNow() {
        return isBuyNow;
    }

    @Override
    public LocalDateTime getFinishDate() {
        return finishDate;
    }

    @Override
    public ProductStatus getStatus() {
        return status;
    }

    @Override
    public User getOwner() {
        return owner;
    }

    @Override
    public List<Bid> getBids() {
        return null;
    }

    public Bid getBestBid() {
        return bestBid;
    }

    public static Product from(Product product) {
        return new ProductTo(product);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ProductTo))
            return false;
        ProductTo other = (ProductTo) obj;
        if (getId() != other.getId())
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;
        if (getTitle() == null) {
            if (other.getTitle() != null)
                return false;
        } else if (!getTitle().equals(other.getTitle()))
            return false;
        return true;
    }
}
