package ru.demi.to;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import ru.demi.model.Role;
import ru.demi.model.User;
import ru.demi.model.UserItem;

public class LoggedUser extends UserItem implements UserDetails {

    public final static LoggedUser GUEST = new LoggedUser(UserItem.GUEST);

    private static final long serialVersionUID = 1L;

    public LoggedUser(User user) {
        super(user);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Role role = getRole() != null ? getRole() : Role.ROLE_GUEST;
        return AuthorityUtils.createAuthorityList(role.toString());
    }

    @Override
    public String getUsername() {
        return getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
