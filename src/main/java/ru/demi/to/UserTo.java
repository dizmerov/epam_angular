package ru.demi.to;

import ru.demi.model.Role;
import ru.demi.model.User;

public class UserTo implements User {

    private Integer id;

    private String name;

    private String billingAddress;

    private String login;

    private Role role;

    public UserTo(){}

    public UserTo(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.billingAddress = user.getBillingAddress();
        this.login = user.getLogin();
        this.role = user.getRole();
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBillingAddress() {
        return billingAddress;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id != null ? id.hashCode() : 0);
        result = prime * result + ((login == null) ? 0 : login.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof UserTo))
            return false;
        UserTo other = (UserTo) obj;
        Integer gotId = other.getId();
        if (getId() != gotId)
            return false;
        if (getLogin() == null) {
            if (other.getLogin() != null)
                return false;
        } else if (!getLogin().equals(other.getLogin()))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "UserTo [id=" + id + ", login=" + login + "]";
    }
}
