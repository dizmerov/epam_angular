package ru.demi.service;

import java.time.LocalDateTime;
import java.util.List;

import ru.demi.model.Bid;
import ru.demi.model.Product;

public interface BidService {

    Bid findById(int bidId, int bidderId);

    Bid findBestBid(int productId, LocalDateTime finishDate);

    List<Bid> findAllByProductId(int productId);

    List<Bid> findAllByBidderId(int bidderId);

    Bid save(Bid bid);

    Bid bidForProduct(int bidderId, int productId, long offer);

    void delete(int id, int bidderId);

    void deactivateBids(List<Product> products);
}
