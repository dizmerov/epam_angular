package ru.demi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.demi.exception.NotValidArgumentException;
import ru.demi.exception.UserNotFoundException;
import ru.demi.model.User;
import ru.demi.repository.UserRepository;
import ru.demi.to.LoggedUser;

@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    UserRepository repository;

    public UserServiceImpl() {}

    public UserServiceImpl(UserRepository userRepository) {
        repository = userRepository;
    }

    @Override
    public User findById(int userId) {
        return repository.findById(userId);
    }

    @Override
    public User findByLogin(String login) {
        return repository.findByLogin(login);
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        if (login == null || login.isEmpty()) {
            throw new NotValidArgumentException();
        }

        if (password == null || password.isEmpty()) {
            throw new NotValidArgumentException();
        }
        return repository.findByLoginAndPassword(login, password);
    }

    @Transactional
    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Transactional
    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public LoggedUser loadUserByUsername(String login) throws UsernameNotFoundException {
        try {
            return new LoggedUser(repository.findByLogin(login));
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException("User " + login + " is not found.");
        }
    }

    @Override
    public boolean exists(String login) {
        try {
            return repository.findByLogin(login) != null;
        } catch (UserNotFoundException e) {
            return false;
        }
    }
}
