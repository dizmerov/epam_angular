package ru.demi.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.demi.exception.BidNotFoundException;
import ru.demi.exception.NotForSaleProductException;
import ru.demi.exception.NotValidArgumentException;
import ru.demi.model.Bid;
import ru.demi.model.BidItem;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.model.UserItem;
import ru.demi.repository.BidRepository;

@Service
public class BidServiceImpl implements BidService {

    @Autowired
    private BidRepository repository;

    @Autowired
    private ProductService productService;

    public BidServiceImpl() {}

    public BidServiceImpl(BidRepository bidRepository, ProductService productService) {
        this.repository = bidRepository;
        this.productService = productService;
    }

    @Override
    public Bid findById(int bidId, int bidderId) {
        return repository.findById(bidId, bidderId);
    }

    @Override
    public Bid findBestBid(int productId, LocalDateTime finishDate) {
        return repository.findBestBid(productId, finishDate);
    }

    @Override
    public List<Bid> findAllByProductId(int productId) {
        return repository.findAllByProductId(productId);
    }

    @Override
    public List<Bid> findAllByBidderId(int bidderId) {
        return repository.findAllByBidderId(bidderId);
    }

    @Transactional
    @Override
    public Bid save(Bid bid) {
        return repository.save(bid);
    }

    @Transactional
    @Override
    public Bid bidForProduct(int bidderId, int productId, long offer) {
        Product product = productService.findById(productId);

        if (product.getStatus() == ProductStatus.NOT_FOR_SALE || product.getOwner().getId() == bidderId || product.isBuyNow()) {
            throw new NotForSaleProductException();
        }

        long prevBest;
        try {
            prevBest = findBestBid(productId, product.getFinishDate()).getOffer() + product.getBidStep();
        } catch (BidNotFoundException e) {
            prevBest = product.getStartPrice() + product.getBidStep();
        }

        if (offer <= prevBest) {
            return null;
        }
        Bid bid = new BidItem(null, new UserItem(bidderId), offer, new ProductItem(productId), LocalDateTime.now(), true);
        return save(bid);
    }

    @Transactional
    @Override
    public void delete(int id, int bidderId) {
        repository.delete(id, bidderId);
    }

    @Transactional
    @Override
    public void deactivateBids(List<Product> products) {
        if (products.isEmpty()) throw new NotValidArgumentException();

        List<Integer> productIds = new ArrayList<>();
        products.forEach(product -> {
            if (product.getId() == null) {
                throw new NotValidArgumentException();
            }
            productIds.add(product.getId());
        });
        repository.deactivateBids(productIds);
    }
}
