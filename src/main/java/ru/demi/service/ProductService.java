package ru.demi.service;

import java.util.List;

import ru.demi.model.Product;

public interface ProductService {

    public static final String ID_PARAM = "id";
    public static final String TITLE_PARAM = "title";
    public static final String DESC_PARAM = "description";
    public static final String STATUS_PARAM = "status";

    Product findById(int id);

    Product findById(int id, int ownerId);

    List<Product> findAll();

    List<Product> findAllByParams(String filterParam, String value, Integer ownerId, boolean withOthers);

    Product save(Product product, int ownerId);

    Product sellProduct(int ownerId, int productId);

    void delete(int id, int ownerId);

    Product buyProduct(int bidderId, int productId, boolean isBuyNow);
}
