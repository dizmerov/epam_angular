package ru.demi.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.demi.exception.AccessDeniedException;
import ru.demi.exception.NotForSaleProductException;
import ru.demi.exception.NotValidArgumentException;
import ru.demi.exception.ProductNotFoundException;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.model.User;
import ru.demi.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private UserService userService;

    public ProductServiceImpl() {}

    public ProductServiceImpl(ProductRepository productRepository, UserService userService) {
        this.repository = productRepository;
        this.userService = userService;
    }

    @Override
    public Product findById(int id) {
        return repository.findById(id);
    }

    @Override
    public Product findById(int id, int ownerId) {
        return repository.findById(id, ownerId);
    }

    @Override
    public List<Product> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Product> findAllByParams(String filterParam, String value, Integer ownerId, boolean withOthers) {
        if (ownerId == null && !withOthers) {
            throw new NotValidArgumentException();
        }
        return repository.findAllByParams(filterParam, value, ownerId, withOthers);
    }

    @Transactional
    @Override
    public Product save(Product product, int ownerId) {
        return repository.save(product, ownerId);
    }

    @Transactional
    @Override
    public Product sellProduct(int ownerId, int productId) {
        Product product;
        try {
            product = findById(productId, ownerId);
        } catch (ProductNotFoundException e) {
            throw new AccessDeniedException();
        }

        LocalDateTime finishDate = LocalDateTime.now().plusHours(product.getHoursToEnd());
        return save(new ProductItem(product, finishDate, ProductStatus.FOR_SALE), ownerId);
    }

    @Transactional
    @Override
    public void delete(int id, int ownerId) {
        repository.delete(id, ownerId);
    }

    @Transactional
    @Override
    public Product buyProduct(int bidderId, int productId, boolean isBuyNow) {
        Product product = findById(productId);
        User bidder = userService.findById(bidderId);

        LocalDateTime finishDate = product.getFinishDate();
        if (product.isBuyNow() != isBuyNow || product.getStatus() != ProductStatus.FOR_SALE || product.getOwner().getId() == bidderId ||
            ((finishDate == null || isBuyNow) ? false : LocalDateTime.now().isBefore(finishDate))) {
            throw new NotForSaleProductException();
        }

        return save(new ProductItem(product, ProductStatus.NOT_FOR_SALE, bidder), product.getOwner().getId());
    }
}
