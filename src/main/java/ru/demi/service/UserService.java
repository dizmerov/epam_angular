package ru.demi.service;

import ru.demi.model.User;

public interface UserService {

    User findById(int userId);

    User findByLoginAndPassword(String login, String password);

    User findByLogin(String login);

    User save(User user);

    void delete(int id);

    boolean exists(String login);
}
