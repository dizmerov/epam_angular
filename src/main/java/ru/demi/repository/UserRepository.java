package ru.demi.repository;

import java.util.List;

import ru.demi.model.User;

public interface UserRepository {
    User findById(int userId);

    User findByLogin(String login);

    User findByLoginAndPassword(String login, String password);

    List<User> findAll();

    User save(User user);

    void delete(int id);
}
