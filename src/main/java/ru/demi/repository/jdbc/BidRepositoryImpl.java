package ru.demi.repository.jdbc;

import static ru.demi.util.DateHelper.DATE_TIME_FORMATTER;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import ru.demi.exception.BidNotFoundException;
import ru.demi.exception.BidsAreNotChangedException;
import ru.demi.exception.BidsAreNotDeletedException;
import ru.demi.model.Bid;
import ru.demi.model.BidItem;
import ru.demi.model.ProductItem;
import ru.demi.model.UserItem;
import ru.demi.repository.BidRepository;
import ru.demi.util.DBHelper;

@Repository
public class BidRepositoryImpl implements BidRepository {

    public static final String SEQ_NAME = "seqBids";

    private static final RowMapper<Bid> ROW_MAPPER = (rs, rowNum) ->  {
        Timestamp timestamp = rs.getTimestamp("offerDate");
        LocalDateTime offerDate = null;
        if (timestamp != null) {
            offerDate = timestamp.toLocalDateTime();
        }
        return new BidItem(rs.getInt("id"), new UserItem(rs.getInt("bidderId")), rs.getLong("offer"), new ProductItem(rs.getInt("productId")), offerDate, rs.getInt("isActive") == 1 ? true : false);
    };

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private SimpleJdbcInsert JdbcInsert;

    @Autowired
    public BidRepositoryImpl(DataSource dataSource) {
        this.JdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("Bids")
                .usingGeneratedKeyColumns("id");
    }

    @Override
    public Bid findById(int id, int bidderId) {
        List<Bid> bids = jdbcTemplate.query("SELECT * FROM Bids WHERE id = ? AND bidderId = ?", ROW_MAPPER, id, bidderId);
        if (bids.isEmpty()) {
            throw new BidNotFoundException();
        }
        return DataAccessUtils.singleResult(bids);
    }

    @Override
    public Bid findBestBid(int productId, LocalDateTime finishDate) {
        List<Bid> bids = null;
        String sql = "SELECT * FROM (SELECT * FROM Bids b INNER JOIN Products p ON b.productId = p.id WHERE b.productId = ? AND p.status = 'FOR_SALE' ";
        if (finishDate != null) {
            sql += "AND b.offerDate < TO_DATE(?, 'dd-mm-yyyy hh24:mi:ss') ";
        }

        if (finishDate != null) {
            bids = jdbcTemplate.query(sql + "ORDER BY offer DESC) WHERE ROWNUM <= 1", ROW_MAPPER, productId, DATE_TIME_FORMATTER.format(finishDate));
        } else {
            bids = jdbcTemplate.query(sql + "ORDER BY offer DESC) WHERE ROWNUM <= 1", ROW_MAPPER, productId);
        }

        if (bids.isEmpty()) {
            throw new BidNotFoundException();
        }
        return DataAccessUtils.singleResult(bids);
    }

    @Override
    public List<Bid> findAllByProductId(int productId) {
        return jdbcTemplate.query("SELECT * FROM Bids WHERE productId = ?", ROW_MAPPER, productId);
    }

    @Override
    public List<Bid> findAllByBidderId(int bidderId) {
        return jdbcTemplate.query("SELECT * FROM Bids WHERE bidderId = ?", ROW_MAPPER, bidderId);
    }

    @Override
    public Bid save(Bid bid) {
        MapSqlParameterSource map = new MapSqlParameterSource()
                .addValue("bidderId", bid.getBidder().getId())
                .addValue("offer", bid.getOffer())
                .addValue("productId", bid.getProduct().getId())
                .addValue("offerDate", bid.getOfferDate() == null? null : DATE_TIME_FORMATTER.format(bid.getOfferDate()))
                .addValue("isActive", bid.isActive()? 1 : 0);

        String sql;
        if (bid.getId() == null) {
            Integer nextId = DBHelper.getNextId(jdbcTemplate, SEQ_NAME);
            map.addValue("id", nextId);
            sql = "INSERT INTO Bids (id, bidderId, offer, productId, %s isActive) VALUES (:id, :bidderId, :offer, :productId, %s :isActive)";

            String offerDateCol = "";
            String offerDateVal = "";
            if (bid.getOfferDate() != null) {
                offerDateCol = "offerDate,";
                offerDateVal = "TO_DATE(:offerDate, 'dd-mm-yyyy hh24:mi:ss'),";
            }

            bid = new BidItem(bid, nextId);
            sql = String.format(sql, offerDateCol, offerDateVal);
        } else {
            map.addValue("id", bid.getId());

            sql = "UPDATE Bids SET bidderId = :bidderId, offer = :offer, productId = :productId, %s isActive = :isActive WHERE id = :id";

            String offerDateHolder = "";
            if (bid.getOfferDate() != null) {
                offerDateHolder = "offerDate = TO_DATE(:offerDate, 'dd-mm-yyyy hh24:mi:ss'),";
            }

            sql = String.format(sql, offerDateHolder);
        }

        if (namedParameterJdbcTemplate.update(sql, map) == 0) {
            throw new BidsAreNotChangedException();
        }

        return bid;
    }

    @Override
    public void delete(int id, int bidderId) {
        int rows = jdbcTemplate.update("DELETE FROM Bids WHERE id = ? AND bidderId = ?", id, bidderId);
        if (rows == 0) {
            throw new BidsAreNotDeletedException();
        }
    }

    @Override
    public void deactivateBids(List<Integer> productIds) {
        int rows = jdbcTemplate.update("UPDATE Bids SET isActive = 0 WHERE productId IN (?) AND isActive = 1",
                StringUtils.collectionToCommaDelimitedString(productIds));
        if (rows == 0) {
            throw new BidsAreNotChangedException();
        }
    }
}
