package ru.demi.repository.jdbc;

import static ru.demi.util.DateHelper.DATE_TIME_FORMATTER;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ru.demi.exception.ProductIsNotDeletedException;
import ru.demi.exception.ProductNotFoundException;
import ru.demi.exception.ProductsAreNotChangedException;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.model.UserItem;
import ru.demi.repository.ProductRepository;
import ru.demi.service.ProductService;
import ru.demi.util.DBHelper;
@Repository
public class ProductRepositoryImpl implements ProductRepository {

    public static final String SEQ_NAME = "seqProducts";

    private static final RowMapper<Product> ROW_MAPPER = (rs, rowNum) ->  {
        Timestamp timestamp = rs.getTimestamp("finishDate");
        LocalDateTime finishDate = null;
        if (timestamp != null) {
            finishDate = timestamp.toLocalDateTime();
        }
        return new ProductItem(rs.getInt("id"), rs.getString("title"), rs.getString("description"), rs.getLong("startPrice"),
                rs.getInt("bidStep"), rs.getInt("hoursToEnd"), rs.getInt("isBuyNow") == 1 ? true : false, finishDate,
                ProductStatus.valueOf(rs.getString("status")), new UserItem(rs.getInt("ownerId")));
    };

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Product findById(int id) {
        return findById(id, null);
    }

    @Override
    public Product findById(int id, Integer ownerId) {
        List<Product> products;
        if (ownerId != null) {
            products = jdbcTemplate.query("SELECT * FROM Products WHERE id = ? AND ownerId = ?", ROW_MAPPER, id, ownerId);
        } else {
            products = jdbcTemplate.query("SELECT * FROM Products WHERE id = ?", ROW_MAPPER, id);
        }

        if (products.isEmpty()) {
            throw new ProductNotFoundException();
        }
        return DataAccessUtils.singleResult(products);
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query("SELECT * FROM Products", ROW_MAPPER);
    }

    @Override
    public List<Product> findAllByParams(String filterParam, String value, Integer ownerId, boolean withOthers) {
        String sql = null;
        Map<String, Object> params = new HashMap<>();
        String filterSql = "1 = 1";
        if (ProductService.STATUS_PARAM.equals(filterParam)) {
            filterSql = String.format("p.%1$s = :%1$s", filterParam);
            params.put(filterParam, value);
        } else if (filterParam != null) {
            String format = "p.%1$s LIKE :%1$s";
            if (filterParam.equals(ProductService.ID_PARAM)) {
                format = "TO_CHAR(p.%1$s) LIKE :%1$s";
            }
            filterSql = String.format(format, filterParam);
            params.put(filterParam, "%" + value + "%");
        }
        String ownerSql = "1 = 1";
        if (!withOthers && ownerId != null) {
            ownerSql = "p.ownerId = :ownerId";
            params.put("ownerId", ownerId);
        }

        sql = String.format("SELECT * FROM Products p WHERE %s AND %s", filterSql, ownerSql);
        return namedParameterJdbcTemplate.query(sql, params, ROW_MAPPER);
    }

    @Override
    public Product save(Product product, int ownerId) {

        MapSqlParameterSource map = new MapSqlParameterSource()
                .addValue("title", product.getTitle())
                .addValue("description", product.getDescription())
                .addValue("startPrice", product.getStartPrice())
                .addValue("bidStep", product.getBidStep())
                .addValue("hoursToEnd", product.getHoursToEnd())
                .addValue("isBuyNow", product.isBuyNow()? 1 : 0)
                .addValue("status", product.getStatus() == null ? null : product.getStatus().toString())
                .addValue("finishDate", product.getFinishDate() == null? null : DATE_TIME_FORMATTER.format(product.getFinishDate()))
                .addValue("ownerId", product.getOwner().getId());

        String sql;
        if (product.getId() == null) {
            Integer nextId = DBHelper.getNextId(jdbcTemplate, SEQ_NAME);
            map.addValue("id", nextId);
            sql = "INSERT INTO Products (id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, status, %s ownerId) "
                    + "VALUES (:id, :title, :description, :startPrice, :bidStep, :hoursToEnd, :isBuyNow, :status, %s :ownerId)";

            String finishDateCol = "";
            String finishDateVal = "";
            if (product.getFinishDate() != null) {
                finishDateCol = "finishDate,";
                finishDateVal = "TO_DATE(:finishDate, 'dd-mm-yyyy hh24:mi:ss'),";
            }

            product = new ProductItem(product, nextId);
            sql = String.format(sql, finishDateCol, finishDateVal);
        } else {
            map.addValue("id", product.getId());
            sql = "UPDATE Products SET title = :title, description = :description, startPrice = :startPrice, bidStep = :bidStep, "
                    + "hoursToEnd = :hoursToEnd, isBuyNow = :isBuyNow, status = :status, %s ownerId = :ownerId "
                    + "WHERE id = ? AND ownerId = ?";

            String finishDateHolder = "";
            if (product.getFinishDate() != null) {
                finishDateHolder = "finishDate = TO_DATE(:finishDate, 'dd-mm-yyyy hh24:mi:ss'),";
            }

            sql = String.format(sql, finishDateHolder);
        }

        if (namedParameterJdbcTemplate.update(sql, map) == 0) {
            throw new ProductsAreNotChangedException();
        }

        return product;
    }

    @Override
    public void delete(int id, int ownerId) {
        int rows = jdbcTemplate.update("DELETE FROM Products WHERE id = ? AND ownerId = ?", id, ownerId);
        if (rows == 0) {
            throw new ProductIsNotDeletedException();
        }
    }
}
