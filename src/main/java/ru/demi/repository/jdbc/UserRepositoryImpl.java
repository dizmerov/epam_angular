package ru.demi.repository.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import ru.demi.exception.UserIsNotDeletedException;
import ru.demi.exception.UserNotFoundException;
import ru.demi.exception.UsersAreNotChangedException;
import ru.demi.model.Role;
import ru.demi.model.User;
import ru.demi.model.UserItem;
import ru.demi.repository.UserRepository;
import ru.demi.util.DBHelper;

@Repository
public class UserRepositoryImpl implements UserRepository {

    public static final String SEQ_NAME = "seqUsers";

    private static final RowMapper<User> ROW_MAPPER = (rs, rowNum) ->  {
        return new UserItem(rs.getInt("id"), rs.getString("name"), rs.getString("billingAddress"), rs.getString("login"),
                rs.getString("password"), Role.valueOf(rs.getString("role")));
    };

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public User findById(int userId) {
        List<User> users = jdbcTemplate.query("SELECT * FROM Users WHERE id = ?", ROW_MAPPER, userId);

        if (users.isEmpty()) {
            throw new UserNotFoundException();
        }

        return DataAccessUtils.singleResult(users);
    }

    @Override
    public User findByLogin(String login) {
        List<User> users = jdbcTemplate.query("SELECT * FROM Users WHERE login = ?", ROW_MAPPER, login);

        if (users.isEmpty()) {
            throw new UserNotFoundException();
        }

        return DataAccessUtils.singleResult(users);
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        List<User> users = jdbcTemplate.query("SELECT * FROM Users WHERE login = ? AND password = ?", ROW_MAPPER, login, password);

        if (users.isEmpty()) {
            throw new UserNotFoundException();
        }

        return DataAccessUtils.singleResult(users);
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("SELECT * FROM Users", ROW_MAPPER);
    }

    @Override
    public User save(User user) {
        MapSqlParameterSource map = new MapSqlParameterSource()
                .addValue("name", user.getName())
                .addValue("billingAddress", user.getBillingAddress())
                .addValue("login", user.getLogin())
                .addValue("password", user.getPassword())
                .addValue("role", user.getRole() == null ? null : user.getRole().toString());

        String sql;
        if (user.getId() == null) {
            sql = "INSERT INTO Users (id, name, billingAddress, login, password, role) VALUES (:id, :name, :billingAddress, :login, :password, :role)";
            Integer nextId = DBHelper.getNextId(jdbcTemplate, SEQ_NAME);
            map.addValue("id", nextId);
            user = new UserItem(user, nextId);
        } else {
            sql = "UPDATE Users SET name = :name, billingAddress = :billingAddress, login = :login, password = :password, role = :role WHERE id = :id";
            map.addValue("id", user.getId());
        }

        if (namedParameterJdbcTemplate.update(sql, map) == 0) {
            throw new UsersAreNotChangedException();
        }
        return user;
    }

    @Override
    public void delete(int id) {
        int rows = jdbcTemplate.update("DELETE FROM Users WHERE id = ?", id);
        if (rows == 0) {
            throw new UserIsNotDeletedException();
        }
    }
}
