package ru.demi.repository;

import java.util.List;

import ru.demi.model.Product;

public interface ProductRepository {
    Product findById(int id);

    Product findById(int id, Integer ownerId);

    List<Product> findAll();

    List<Product> findAllByParams(String filterParam, String value, Integer ownerId, boolean withOthers);

    Product save(Product product, int ownerId);

    void delete(int id, int ownerId);
}