package ru.demi.repository;

import java.time.LocalDateTime;
import java.util.List;

import ru.demi.model.Bid;

public interface BidRepository {
    Bid findById(int bidId, int bidderId);

    Bid findBestBid(int productId, LocalDateTime finishDate);

    List<Bid> findAllByProductId(int productId);

    List<Bid> findAllByBidderId(int bidderId);

    Bid save(Bid bid);

    void delete(int id, int bidderId);

    void deactivateBids(List<Integer> productIds);
}
