package ru.demi.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import ru.demi.exception.UserIsNotDeletedException;
import ru.demi.exception.UserNotFoundException;
import ru.demi.model.User;
import ru.demi.model.UserItem;
import ru.demi.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    public static final String SEQ_NAME = "seqUsers";
    public static final String TABLE_NAME = "Users";

    @PersistenceContext
    private EntityManager em;

    @Override
    public User findById(int userId) {
        User user = em.find(UserItem.class, userId);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    public User findByLogin(String login) {
        try {
            User user = em.createQuery("SELECT u FROM UserItem u WHERE u.login = :login", UserItem.class)
                    .setParameter("login", login).getSingleResult();

            if (user == null) {
                throw new UserNotFoundException();
            }
            return user;

        } catch (NoResultException e) {
            throw new UserNotFoundException(e);
        }
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        try {
            User user = em
                    .createQuery("SELECT u FROM UserItem u WHERE u.login = :login AND u.password = :password",
                            UserItem.class)
                    .setParameter("login", login).setParameter("password", password).getSingleResult();

            if (user == null) {
                throw new UserNotFoundException();
            }
            return user;
        } catch (NoResultException e) {
            throw new UserNotFoundException(e);
        }
    }

    @Override
    public List<User> findAll() {
        return em.createQuery("SELECT u FROM UserItem u", User.class).getResultList();
    }

    @Override
    public User save(User user) {
        if (user.getId() == null) {
            em.persist(user);
        } else {
            user = em.merge(user);
        }
        return user;
    }

    @Override
    public void delete(int id) {
        int rows = em.createQuery("DELETE FROM UserItem u WHERE u.id = :id")
            .setParameter("id", id)
            .executeUpdate();
        if (rows == 0) {
            throw new UserIsNotDeletedException();
        }
    }
}
