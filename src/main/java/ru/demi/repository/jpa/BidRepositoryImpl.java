package ru.demi.repository.jpa;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import ru.demi.exception.BidNotFoundException;
import ru.demi.exception.BidsAreNotChangedException;
import ru.demi.exception.BidsAreNotDeletedException;
import ru.demi.model.Bid;
import ru.demi.model.BidItem;
import ru.demi.repository.BidRepository;

@Repository
public class BidRepositoryImpl implements BidRepository {

    public static final String SEQ_NAME = "seqBids";

    public static final String TABLE_NAME = "Bids";

    @PersistenceContext
    private EntityManager em;

    @Override
    public Bid findById(int id, int bidderId) {

        TypedQuery<BidItem> query = em.createQuery("SELECT b FROM BidItem b WHERE b.id = :id AND b.bidder.id = :bidderId", BidItem.class);
        query.setParameter("id", id);
        query.setParameter("bidderId", bidderId);
        query.setMaxResults(1);
        List<BidItem> bids = query.getResultList();
        if (bids.isEmpty()) {
            throw new BidNotFoundException();
        }
        return bids.get(0);
    }

    @Override
    public Bid findBestBid(int productId, LocalDateTime finishDate) {
        String jpql = "SELECT b FROM BidItem b WHERE b.product.id = :productId AND b.product.status = 'FOR_SALE' ";
        if (finishDate != null) {
            jpql += "AND b.offerDate < :finishDate ";
        }
        TypedQuery<BidItem> query = em.createQuery(jpql + "ORDER BY b.offer DESC", BidItem.class);
        query.setParameter("productId", productId);
        if (finishDate != null) {
            query.setParameter("finishDate", finishDate);
        }
        query.setMaxResults(1);
        List<BidItem> list = query.getResultList();
        if (list.isEmpty()) {
            throw new BidNotFoundException();
        }
        return list.get(0);
    }

    @Override
    public List<Bid> findAllByProductId(int productId) {
        return em.createQuery("SELECT b FROM BidItem b WHERE b.product.id = :productId", Bid.class)
                .setParameter("productId", productId)
                .getResultList();
    }

    @Override
    public List<Bid> findAllByBidderId(int bidderId) {
        return em.createQuery("SELECT b FROM BidItem b WHERE b.bidder.id = :bidderId", Bid.class)
                .setParameter("bidderId", bidderId)
                .getResultList();
    }

    @Override
    public Bid save(Bid bid) {

        if (bid.getId() == null) {
            em.persist(bid);
        } else {
            bid = em.merge(bid);
        }
        return bid;
    }

    @Override
    public void delete(int id, int bidderId) {
        int rows = em.createQuery("DELETE FROM BidItem b WHERE b.id = :id AND b.bidder.id = :bidderId")
            .setParameter("id", id)
            .setParameter("bidderId", bidderId)
            .executeUpdate();
        if (rows == 0) {
            throw new BidsAreNotDeletedException();
        }
    }

    @Override
    public void deactivateBids(List<Integer> productIds) {
        int rows = em.createQuery("UPDATE BidItem b SET b.isActive = :isActive WHERE b.product.id IN :productIds AND b.isActive = true")
            .setParameter("isActive", false)
            .setParameter("productIds", productIds)
            .executeUpdate();
        if (rows == 0) {
            throw new BidsAreNotChangedException();
        }
    }
}
