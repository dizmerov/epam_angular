package ru.demi.repository.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import ru.demi.exception.ProductIsNotDeletedException;
import ru.demi.exception.ProductIsNotSavedException;
import ru.demi.exception.ProductNotFoundException;
import ru.demi.model.Product;
import ru.demi.model.ProductItem;
import ru.demi.model.ProductStatus;
import ru.demi.model.User;
import ru.demi.model.UserItem;
import ru.demi.repository.ProductRepository;
import ru.demi.service.ProductService;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    public static final String SEQ_NAME = "seqProducts";
    public static final String TABLE_NAME = "Products";

    @PersistenceContext
    private EntityManager em;

    @Override
    public Product findById(int id) {
        Product product = em.find(ProductItem.class, id);
        if (product == null) {
            throw new ProductNotFoundException();
        }
        return product;
    }

    @Override
    public Product findById(int id, Integer ownerId) {

        if (ownerId == null) {
            return findById(id);
        }
        TypedQuery<ProductItem> query = em.createQuery("SELECT p FROM ProductItem p WHERE p.id = :id AND p.owner.id = :ownerId", ProductItem.class);
        query.setParameter("id", id);
        query.setParameter("ownerId", ownerId);
        query.setMaxResults(1);
        List<ProductItem> products = query.getResultList();
        if (products.isEmpty()) {
            throw new ProductNotFoundException();
        }
        return products.get(0);
    }

    @Override
    public List<Product> findAll() {
        return em.createQuery("SELECT p FROM ProductItem p", Product.class)
                .getResultList();
    }

    @Override
    public List<Product> findAllByParams(String filterParam, String value, Integer ownerId, boolean withOthers) {
        String sql = null;

        Map<String, Object> params = new HashMap<>();
        String filterSql = "1 = 1";
        if (ProductService.STATUS_PARAM.equals(filterParam)) {
            filterSql = String.format("p.%1$s = :%1$s", filterParam);
            params.put(filterParam, value);
        } else if (filterParam != null) {
            String format = "p.%1$s LIKE :%1$s";
            if (filterParam.equals(ProductService.ID_PARAM)) {
                format = "STR(p.%1$s) LIKE :%1$s";
            }
            filterSql = String.format(format, filterParam);
            params.put(filterParam, "%" + value + "%");
        }
        String ownerSql = "1 = 1";
        if (!withOthers && ownerId != null) {
            ownerSql = "p.owner.id = :ownerId";
            params.put("ownerId", ownerId);
        }

        sql = String.format("SELECT p FROM ProductItem p WHERE %s AND %s", filterSql, ownerSql);
        TypedQuery<Product> query = em.createQuery(sql, Product.class);
        for (Entry<String, Object> entry : params.entrySet()) {
            String key = entry.getKey();
            if (key.equals(ProductService.STATUS_PARAM)) {
                query.setParameter(key, ProductStatus.valueOf((String) entry.getValue()));
            } else {
                query.setParameter(key, entry.getValue());
            }
        }

        return query.getResultList();
    }

    @Override
    public Product save(Product product, int ownerId) {
        if (product.getOwner() == null) {
            User ref = em.getReference(UserItem.class, ownerId);
            product = new ProductItem(product, ref);
        }

        if (product.getId() == null) {
            em.persist(product);
        } else {
            try{
                findById(product.getId(), ownerId);
                product = em.merge(product);
            } catch (ProductNotFoundException e) {
                throw new ProductIsNotSavedException();
            }
        }
        return product;
    }

    @Override
    public void delete(int id, int ownerId) {
        int rows = em.createQuery("DELETE FROM ProductItem p WHERE p.id = :id AND p.owner.id = :ownerId")
                .setParameter("id", id)
                .setParameter("ownerId", ownerId)
                .executeUpdate();
        if (rows == 0) {
            throw new ProductIsNotDeletedException();
        }
    }
}
