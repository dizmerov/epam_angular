function RegisterFormCtrl($http, $location) {

	this.newUser = {};
	this.errors = {};

	this.register = function() {
		var that = this;
		$http.post('/register', this.newUser).then(function(data) {
			that.newUser = {};
			$location.path('/login');
		}, function(error) {
			if (error.data && error.data.errors) {
				that.errors = error.data.errors;
			}
			alert('Error login!');
		});
	};

	this.onChange = function() {
		this.errors = {};
	};

	this.onResetBtnClick = function() {
		this.newUser = {};
	}
}