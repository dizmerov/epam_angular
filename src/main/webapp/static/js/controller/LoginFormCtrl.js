function LoginFormCtrl($rootScope, $http, $location, userService) {
    this.userData = {};
    this.isNotValidPassword = false;
    
    this.onChange = function() {
    	this.isNotValidPassword = false;
    }
    
    this.login = function() {
    	var that = this;
        $http
            .post(
                '/login',
                $.param(this.userData),
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
            )
            .then(function() {
                userService.loadUser(that.userData.login)
                	.then(function(data) {
                		userService.setUser(data);
                        $location.path('/');
                	}, function() {
                		alert('User with this login is non-existent.');
                	});
            }, function () {
            	that.isNotValidPassword = true;
            	angular.element('#pwd').addClass('ng-invalid').removeClass('ng-valid');
            });
    }
}