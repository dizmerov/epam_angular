function ProductFormCtrl($location, productRest, product) {
	this.product = product;
	this.errors = {};

	this.add = function() {
		var that = this;
		productRest.create(this.product).$promise.then(function(data) {
			$location.path('/products');
		}, function(error) {
			if (error.data && error.data.errors) {
				that.errors = error.data.errors;
			}
			alert('Error add!');
		});
	};

	this.edit = function() {
		var that = this;
		productRest.save(this.product).$promise.then(function(data) {
			$location.path('/products');
		}, function(error) {
			if (error.data && error.data.errors) {
				that.errors = error.data.errors;
			}
			alert('Error edit!');
		});
	};

	this.onChange = function() {
		this.errors = {};
	};

	this.onResetBtnClick = function() {
		this.product = {};
	};

	this.onFormSubmit = function() {
		if (this.product.id && angular.isNumber(this.product.id)
				&& isFinite(this.product.id)) {
			this.edit();
		} else {
			this.add();
		}
	};
}