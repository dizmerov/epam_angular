function Products($location, productRest) {
	return {
		replace : true,
		restrict : 'E',
		templateUrl : 'static/views/products.html',
		controller : 'ProductsCtrl',
		controllerAs : 'productsCtrl',
		scope : {
			products : '='
		},
		link : function(scope, element, attrs, controller) {

			scope.onShowAllBtnClick = function() {
				productRest.getProducts({
					'withOthers' : 'on'
				}).$promise.then(function(data) {
					if (data) {
						scope.products = data;
					}
					scope.isShowAll = true;
				}, function() {
					alert('Error loading products!');
				});
			};

			scope.onShowMyItemsBtnClick = function() {
				var params, 
					param = scope.param, 
					value = scope.keyword;
				if (param && value) {
					params = {};
					params[param] = value;
				}
				productRest.getProducts(params).$promise.then(function(data) {
					if (data) {
						scope.products = data;
					}
					scope.isShowAll = false;
				}, function() {
					alert('Error loading products!');
				});
			};

			scope.onSearchFormSubmit = function(param, value) {
				if (!param || !value)
					return;
				var params = {
					'withOthers' : 'on'
				};
				params[param] = value;
				productRest.getProducts(params).$promise.then(function(data) {
					if (data) {
						scope.products = data;
					}
				}, function() {
					alert('Error loading products!');
				});
			};

			scope.onSellBtnClick = function() {
				var els = angular
						.element('#products tr input:checked.checkbox-for-sale');
				var ids = [];

				angular.forEach(els, function(el) {
					ids.push(angular.element(el).data('id'));
				}, ids);

				ids.forEach(function(id) {
					productRest.sellProduct({'id': id}).$promise.then(function() {
						for (var i = 0, l = scope.products.length; i < l; ++i) {
							if (scope.products[i].id == id) {
								scope.products[i].status = 'FOR_SALE';
								break;
							}
						}
					}, function() {
						alert('Error selling products!');
					});
				});
			};

			scope.onAddBtnClick = function() {
				$location.path('/products/add');
			};

			scope.onEditBtnClick = function($event) {
				$location.path('/products/' + getId($event));
			};

			scope.onDeleteBtnClick = function($event) {
				var id = getId($event);
				var i, l;

				productRest.deleteProduct({'id': id}).$promise.then(function() {
					for (i = 0, l = scope.products.length; i < l; ++i) {
						if (scope.products[i].id == id) {
							break;
						}
					}
					scope.products.splice(i, 1);

				}, function() {
					alert('Error deleting products!');
				});
			};

			scope.onBidBtnClick = function($event) {
				var id = getId($event);
				var offer = getElByEvent($event).closest('form').find(
						'.input-offer').val();

				if (!offer || $.trim(offer).length == 0)
					return;
				productRest.bidForProduct({'id': id, offer: $.trim(offer)}).$promise.then(
						function() {
							scope.onShowAllBtnClick();
						}, function() {
							alert('Error bid for product!');
						});
			};

			scope.onBuyBtnClick = function($event) {
				var id = getId($event);

				productRest.buyProduct({'id': id}).$promise.then(function() {
					scope.onShowAllBtnClick();
				}, function() {
					alert('Error buying product!');
				});
			};

			function getElByEvent($event) {
				return $($event.currentTarget || $event.srcElement);
			}

			function getId($event) {
				return getElByEvent($event).closest('tr').data('id');
			}
		}
	}
}