function NavBar() {
	return {
		restrict : 'E',
		templateUrl : 'static/views/navbar.html',
		controller : [ '$rootScope', '$scope', '$http', '$location',
				'userService',
				function($rootScope, $scope, $http, $location, userService) {

					$scope.userService = userService;

					$scope.isActive = function(page) {
						var current = $location.path().substring(1);
						return page === current;
					}

					$scope.onLogoutClick = function() {
						$http.get('/logout').then(function() {
							userService.setUser(null);
							$location.path('/login');
						}, function() {
							alert('Error logout!');
						});
					}
				} ]
	}
}