function PasswordRepeatValidation() {
	return {
		require : 'ngModel',
		link : function(scope, elm, attrs, ctrl) {

			ctrl.$validators.passwordRepeat = function(modelValue, viewValue) {
				if (ctrl.$isEmpty(modelValue)) {
					return true;
				}
				var originPwd = angular.element(attrs.passwordRepeat).val();
				if (originPwd === modelValue) {
					return true;
				}

				return false;
			};
		}
	};
}