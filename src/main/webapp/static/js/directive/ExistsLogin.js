function ExistsLogin($q, userRest) {
	return {
		require : 'ngModel',
		link : function(scope, elm, attrs, ctrl) {

			ctrl.$asyncValidators.existsLogin = function(modelValue, viewValue) {

				if (ctrl.$isEmpty(modelValue)) {
					return $q.resolve();
				}

				return $q(function(resolve, reject) {
					userRest.exists({login: modelValue}).$promise.then(
							function(d) {
								if (d.data && d.data.message === true) {
									resolve();
								} else {
									reject();
								}
							}, function() {
								reject();
							});
				});
			};
		}
	};
}