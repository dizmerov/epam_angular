function NotExistsLogin($q, userRest) {
	return {
		require : 'ngModel',
		link : function(scope, elm, attrs, ctrl) {

			ctrl.$asyncValidators.notExistsLogin = function(modelValue,
					viewValue) {

				if (ctrl.$isEmpty(modelValue)) {
					return $q.resolve();
				}

				return $q(function(resolve, reject) {
					userRest.exists({login: modelValue}).$promise.then(
							function(d) {
								if (d.data && d.data.message === false) {
									resolve();
								} else {
									reject();
								}
							}, function() {
								reject();
							});
				});
			};
		}
	};
}