function UserService(userRest) {
	var user = null;
	return {
		getUser : function() {
			var u = JSON.parse(window.localStorage.getItem('user'));
			if (user === null && u) {
				user = u;
			}
			return user;
		},
		setUser : function(u) {
			window.localStorage.setItem('user', JSON.stringify(u));
			return user = u;
		},
		isNull : function() {
			return this.getUser() === null;
		},
		loadUser : function(login) {
			return userRest.by({login: login}).$promise;
		}
	};
}