function ProductRest($resource) {
	return $resource('products', {}, {
				'getProducts' : {
					method : 'GET', 
					params: {
						withOthers: '@withOthers',
						id: '@id',
						title: '@title',
						desc: '@desc'
					},
					isArray: true
				},
				'create' : {method: 'PUT'},
				'getProductById' : {method : 'GET', url: 'products/:id', params: {id: '@id'}},
				'deleteProduct' : {method : 'DELETE', url: 'products/:id', params: {id: '@id'}},
				'sellProduct' : {method : 'POST', url: 'products/:id/sell', params: {id: '@id'}},
				'buyProduct' : {method : 'POST', url: 'products/:id/buy', params: {id: '@id'}},
				'bidForProduct' : {
					method : 'POST', 
					url: 'bids/products/:id',
					params: {
						id: '@id',
						offer : '@offer'
					}
				}
			});
}