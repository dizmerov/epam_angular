function UserRest($resource) {
	return $resource('users', {}, {
				'exists' : {method : 'GET', url: 'users/exists', params: {login: '@login'}},
				'by' : {method : 'GET', url: 'users/by', params: {login: '@login'}}
			});
}