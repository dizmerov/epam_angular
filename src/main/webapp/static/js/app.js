(function() {
    "use strict";
    angular.module('app', ['ngRoute', 'ngResource'])
        .controller('ProductsCtrl', ['$scope', 'userService', ProductsCtrl])
        .controller('LoginFormCtrl', ['$rootScope', '$http', '$location', 'userService', LoginFormCtrl])
        .controller('RegisterFormCtrl', ['$http', '$location', RegisterFormCtrl])
        .controller('ProductFormCtrl', ['$location', 'productRest', 'product', ProductFormCtrl])
        .controller('RootCtrl', ['$scope', 'productList', function RootCtrl($scope, productList) {
        	$scope.productList = productList;
        }])
        .directive('navBar', NavBar)
        .directive('products', ['$location', 'productRest', Products])
        .directive('notExistsLogin', ['$q', 'userRest', NotExistsLogin])
        .directive('existsLogin', ['$q', 'userRest', ExistsLogin])
        .directive('passwordRepeat', PasswordRepeatValidation)
        .factory('userService', ['userRest', UserService])
        .factory('productRest', ['$resource', ProductRest])
        .factory('userRest', ['$resource', UserRest])
        .config(function($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    template: '<products products="productList"></products>',
                    controller: 'RootCtrl',
                    controllerAs: 'rootCtrl',
                    resolve: {
                    	productList: ['$q', 'productRest', ProductsCtrl.getProducts]
                    }
                })
                .when('/register', {
                    templateUrl: 'static/views/register-form.html',
                    controller: 'RegisterFormCtrl',
                    controllerAs: 'registerFormCtrl'
                })
                .when('/login', {
                    templateUrl: 'static/views/login-form.html',
                    controller: 'LoginFormCtrl',
                    controllerAs: 'loginFormCtrl'
                })
                .when('/products/add', {
                    templateUrl: 'static/views/product-form.html',
                    controller: 'ProductFormCtrl',
                    controllerAs: 'productFormCtrl',
                    resolve: {
                    	product: function () {
                    		return {};
                    	}
                    }
                })
                .when('/products/:id', {
                    templateUrl: 'static/views/product-form.html',
                    controller: 'ProductFormCtrl',
                    controllerAs: 'productFormCtrl',
                    resolve: {
                    	product: ['$q', '$route', 'productRest', ProductsCtrl.getProduct]
                    }
                })
                .otherwise({
                    redirectTo: '/'
                });
        });
})();