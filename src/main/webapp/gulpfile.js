// Various helper modules
var gulp = require("gulp");
var plug = require("gulp-load-plugins")();
var source = "static/js/**/*.js";
var out = "app.min.js";
var dest = "./static";

gulp.task("help", plug.taskListing);

gulp.task("minify", function () {
	return gulp
		.src(source)
        .pipe(plug.concat(out))
		.pipe(plug.ngAnnotate({add: true, single_quotes: false}))
		.pipe(plug.uglify({mangle: true}))
		.pipe(gulp.dest(dest));
});

gulp.task("default", ["minify"]);