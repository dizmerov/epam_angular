DELETE FROM Users;
DELETE FROM Bids;
DELETE FROM Products;
CALL resetSequence('seqUsers', 6);
CALL resetSequence('seqBids', 5);
CALL resetSequence('seqProducts', 5);

INSERT INTO Users (id, name, billingAddress, login, password, role) VALUES 
	(1, 'user1', 'address of user 1', 'login1', 'password1', 'ROLE_USER');
INSERT INTO Users (id, name, billingAddress, login, password, role) VALUES
	(2, 'user2', 'address of user 2', 'login2', 'password2', 'ROLE_USER');
INSERT INTO Users (id, name, billingAddress, login, password, role) VALUES
	(3, 'user3', 'address of user 3', 'login3', 'password3', 'ROLE_USER');
INSERT INTO Users (id, name, billingAddress, login, password, role) VALUES
	(4, 'user4', 'address of user 4', 'login4', 'password4', 'ROLE_USER');
INSERT INTO Users (id, name, billingAddress, login, password, role) VALUES
	(5, 'admin', 'admin address', 'admin', 'admin', 'ROLE_ADMIN');

INSERT INTO Products (id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, status, finishDate, ownerId) VALUES
	(1, 'product1', 'desc for product1', 10000, 1000, 240, 0, 'NOT_FOR_SALE', null, 1);
INSERT INTO Products (id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, status, finishDate, ownerId) VALUES
	(2, 'product2', 'desc for product2', 12000, 2000, 120, 0, 'NOT_FOR_SALE', null, 2);
INSERT INTO Products (id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, status, finishDate, ownerId) VALUES
	(3, 'product3', 'product3 for sale', 10000, 0, 0, 0, 'FOR_SALE', sysdate, 3);
INSERT INTO Products (id, title, description, startPrice, bidStep, hoursToEnd, isBuyNow, status, finishDate, ownerId) VALUES
	(4, 'product4', 'product4 for sale', 12000, 0, 0, 0, 'FOR_SALE', sysdate, 3);

INSERT INTO Bids (id, bidderId, offer, productId, offerDate) VALUES
	(1, 1, 11000, 3, sysdate - interval '1' day);
INSERT INTO Bids (id, bidderId, offer, productId, offerDate) VALUES
	(2, 2, 12000, 3, sysdate - interval '2' day);
INSERT INTO Bids (id, bidderId, offer, productId, offerDate) VALUES
	(3, 1, 15000, 4, sysdate - interval '3' day);
INSERT INTO Bids (id, bidderId, offer, productId, offerDate) VALUES
	(4, 2, 14000, 4, sysdate - interval '4' day);